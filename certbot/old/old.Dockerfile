FROM certbot/certbot

RUN apk update && apk add --no-cache \
    cron \
    python3 \
    py3-pip

RUN pip install certbot-dns-cloudflare

COPY renew.sh /usr/local/bin/renew.sh
COPY entrypoint.sh /entrypoint.sh
COPY crontab /etc/cron.d/certbot-renew

RUN chmod +x /usr/local/bin/renew.sh
RUN chmod +x /entrypoint.sh

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/certbot-renew

# Apply cron job
RUN crontab /etc/cron.d/certbot-renew

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

ENTRYPOINT ["/entrypoint.sh"]