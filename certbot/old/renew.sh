#!/bin/sh

certbot certonly \
  --dns-cloudflare \
  --dns-cloudflare-credentials /run/secrets/certbot-cloudflare-token \
  --dns-cloudflare-propagation-seconds 60 \
  --email your-email@example.com \
  --agree-tos \
  --no-eff-email \
  -d *.starblast.info \
  --non-interactive

certbot renew --dns-cloudflare --dns-cloudflare-credentials /run/secrets/certbot-cloudflare-token

# Restart services if necessary (e.g., nginx, apache)
# service nginx reload
# service apache2 reload


certbot certonly --dns-cloudflare --dns-cloudflare-credentials /run/secrets/certbot-cloudflare-token --email warrentarmstrong@gmail.com --agree-tos --no-eff-email -d *.starblast.info