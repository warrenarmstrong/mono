FROM python:3.11-slim

RUN pip install --upgrade pip

COPY code-location-requirements.txt /requirements.txt

RUN pip install -r /requirements.txt

# RUN pip install \
#     dagster \
#     dagster-postgres \
#     dagster-docker \
#     sqlalchemy \
#     psycopg2-binary \
#     polars \
#     duckdb==1.0.1-dev3777 \
#     sqlescapy \
#     boto3 \
#     pyarrow

WORKDIR /opt/dagster/app
COPY mono/ /opt/dagster/app/

# RUN pip install --upgrade pip

# RUN pip install -e .

EXPOSE 4000

# CMD ["dagster", "api", "grpc", "-h", "0.0.0.0", "-p", "4000", "-m", "mono.starblast_info"]