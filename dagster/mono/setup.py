from setuptools import find_packages, setup  # type: ignore

setup(
    name="mono",
    packages=find_packages(exclude=["mono_tests"]),
    install_requires=[
        "dagster",
        "dagster-postgres",
        "dagster-docker",
        "sqlalchemy",
        "psycopg2-binary",
        "polars",
        "duckdb",
        "sqlescapy",
        "boto3",
        "pyarrow",
    ],
    extras_require={"dev": ["dagster-webserver", "pytest"]},
)
