from sqlalchemy.sql import text

from dagster import OpExecutionContext, job, op

from ..shared.resources import PostgresResource  # type: ignore
from .assets import create_system_system_listener_temp_table
from .partitions import starblast_info_system_partition_def


@op
def starblast_info_pg_prune_op(
    context: OpExecutionContext, starblast_info_pg: PostgresResource
) -> None:
    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        table_names = [
            "alien_scd",
            "player_scd",
            "station_module_scd",
            "team_scd",
            "player",
            "station_module",
            "team",
            "system",
        ]

        for table_name in table_names:
            query = f"""
                DELETE FROM listener.{table_name}
                WHERE system_listener_id IN (
                    SELECT system_listener_id
                    FROM system_system_listener
                    WHERE system_id = '{context.partition_key}'
                );
            """

            context.log.info(query)

            connection.execute(text(query))

        connection.commit()


@job(partitions_def=starblast_info_system_partition_def, tags={"dagster/priority": 2})
def starblast_info_pg_prune_job() -> None:
    starblast_info_pg_prune_op()
