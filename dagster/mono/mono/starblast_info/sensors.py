from dagster import (AssetKey, DefaultSensorStatus,
                     MultiAssetSensorEvaluationContext, RunRequest,
                     SensorEvaluationContext, SensorResult, multi_asset_sensor,
                     sensor)
from sqlalchemy.sql import text

from ..shared.resources import PostgresResource  # type: ignore
from .assets import (create_system_system_listener_temp_table,
                     starblast_info_materialize_silver_job,
                     starblast_info_persist_system_job)
from .jobs import starblast_info_pg_prune_job
from .partitions import starblast_info_system_partition_def


@sensor(
    job=starblast_info_persist_system_job, default_status=DefaultSensorStatus.STOPPED
)
def starblast_info_system_sensor(
    context: SensorEvaluationContext, starblast_info_pg: PostgresResource
) -> SensorResult:
    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        query = """
            SELECT ssl.system_id
            FROM
                system_system_listener AS ssl
                INNER JOIN listener.system AS s
                    ON ssl.system_listener_id = s.system_listener_id
            GROUP BY ssl.system_id
            HAVING EXTRACT(EPOCH FROM NOW()) - MAX(s.latest_ts) > 300;
        """

        system_ids = [str(row[0]) for row in connection.execute(text(query))]

        run_requests = [
            RunRequest(run_key=system_id, partition_key=system_id)
            for system_id in system_ids
        ]

        dynamic_partition_requests = [
            starblast_info_system_partition_def.build_add_request(system_ids)
        ]

        return SensorResult(
            run_requests=run_requests,
            dynamic_partitions_requests=dynamic_partition_requests,
        )


@multi_asset_sensor(
    monitored_assets=[
        AssetKey(["starblast_info", "bronze", "landing", "system"]),
        AssetKey(["starblast_info", "bronze", "landing", "team"]),
        AssetKey(["starblast_info", "bronze", "landing", "station_module"]),
        AssetKey(["starblast_info", "bronze", "landing", "player"]),
        AssetKey(["starblast_info", "bronze", "landing", "team_scd"]),
        AssetKey(["starblast_info", "bronze", "landing", "station_module_scd"]),
        AssetKey(["starblast_info", "bronze", "landing", "player_scd"]),
        AssetKey(["starblast_info", "bronze", "landing", "alien_scd"]),
    ],
    jobs=[starblast_info_pg_prune_job, starblast_info_materialize_silver_job],
    default_status=DefaultSensorStatus.STOPPED,
)
def starblast_info_bronze_landing_asset_sensor(
    context: MultiAssetSensorEvaluationContext,
) -> SensorResult:
    run_requests = []
    for (
        partition,
        materializations_by_asset,
    ) in context.latest_materialization_records_by_partition_and_asset().items():
        if set(materializations_by_asset.keys()) == set(context.asset_keys):
            run_requests.append(
                RunRequest(
                    partition_key=partition, job_name="starblast_info_pg_prune_job"
                )
            )
            run_requests.append(
                RunRequest(
                    partition_key=partition,
                    job_name="starblast_info_materialize_silver_job",
                )
            )
            for asset_key, materialization in materializations_by_asset.items():
                context.advance_cursor({asset_key: materialization})
    return SensorResult(run_requests=run_requests)
