import polars as pl
from dagster import AssetExecutionContext, AssetIn, AssetKey, asset

from ...shared.resources import SQL  # type: ignore
from ..partitions import starblast_info_system_partition_def
from .util import get_query


@asset(
    name="system",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        )
    },
    code_version="1",
)
def deduped_system(
    context: AssetExecutionContext, bronze_landing_system: pl.DataFrame
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "system"]),
        bronze_landing_system=bronze_landing_system,
    )


@asset(
    name="team",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_team": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "team"])
        ),
    },
    code_version="1",
)
def deduped_team(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_team: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "team"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_team=bronze_landing_team,
    )


@asset(
    name="station_module",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_station_module": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "station_module"])
        ),
    },
    code_version="1",
)
def deduped_station_module(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_station_module: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "station_module"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_station_module=bronze_landing_station_module,
    )


@asset(
    name="player",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_player": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "player"])
        ),
    },
    code_version="1",
)
def deduped_player(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_player: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "player"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_player=bronze_landing_player,
    )


@asset(
    name="team_scd",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_team_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "team_scd"])
        ),
    },
    code_version="1",
)
def deduped_team_scd(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_team_scd: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "team_scd"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_team_scd=bronze_landing_team_scd,
    )


@asset(
    name="station_module_scd",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_station_module_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "station_module_scd"])
        ),
    },
    code_version="1",
)
def deduped_station_module_scd(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_station_module_scd: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "station_module_scd"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_station_module_scd=bronze_landing_station_module_scd,
    )


@asset(
    name="player_scd",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_player_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "player_scd"])
        ),
    },
    code_version="1",
)
def deduped_player_scd(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_player_scd: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "player_scd"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_player_scd=bronze_landing_player_scd,
    )


@asset(
    name="alien_scd",
    key_prefix=["starblast_info", "bronze", "deduped"],
    partitions_def=starblast_info_system_partition_def,
    ins={
        "bronze_landing_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "system"])
        ),
        "bronze_landing_alien_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "landing", "alien_scd"])
        ),
    },
    code_version="1",
)
def deduped_alien_scd(
    context: AssetExecutionContext,
    bronze_landing_system: pl.DataFrame,
    bronze_landing_alien_scd: pl.DataFrame,
) -> SQL:
    return SQL(
        get_query(["bronze", "deduped", "alien_scd"]),
        bronze_landing_system=bronze_landing_system,
        bronze_landing_alien_scd=bronze_landing_alien_scd,
    )
