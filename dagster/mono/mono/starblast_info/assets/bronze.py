import polars as pl
from sqlalchemy.engine.base import Connection
from sqlalchemy.sql import text

from dagster import AssetExecutionContext, asset

from ...shared.resources import SQL, PostgresResource  # type: ignore
from ..partitions import starblast_info_system_partition_def
from .util import create_system_system_listener_temp_table, get_query


def is_playerless_system(system_id: str, connection: Connection) -> bool:
    query = get_query(["is_playerless_system"]).format(
        system_id=system_id
    )

    result =  pl.read_database(
        query=query,
        connection=connection,
    ).to_numpy()[0, 0].item()

    return result


@asset(
    name="system",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_system(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "system"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            raise Exception("Empty dataframe in bronze asset")

    return SQL("select * from $df", df=df)


@asset(
    name="team",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_team(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "team"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            raise Exception("Empty dataframe in bronze asset")

    return SQL("select * from $df", df=df)


@asset(
    name="station_module",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_station_module(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "station_module"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            raise Exception("Empty dataframe in bronze asset")

    return SQL("select * from $df", df=df)


@asset(
    name="player",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_player(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "player"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
            schema_overrides={
                "system_id": pl.Utf8,
                "system_listener_id": pl.Utf8,
                "team_sb_id": pl.Int16,
                "player_sb_id": pl.Int16,
                "name": pl.Utf8,
                "hue": pl.Int32,
                "custom_badge": pl.Utf8,
                "custom_finish": pl.Utf8,
                "custom_laser": pl.Utf8,
                "custom_hue": pl.Int32
            }
        )

        if df.shape[0] == 0:
            if not is_playerless_system(context.partition_key, connection):
                raise Exception("Empty dataframe in bronze asset")

    return SQL("select * from $df", df=df)


@asset(
    name="team_scd",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_team_scd(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "team_scd"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            raise Exception("Empty dataframe in bronze asset")

    use_dictionary = [
        "system_id",
        "system_listener_id",
        "team_sb_id",
        "is_open",
        "base_level",
        "is_deleted",
    ]

    column_encoding = {"ts": "DELTA_BINARY_PACKED", "crystals": "DELTA_BINARY_PACKED"}

    return SQL(
        "select * from $df",
        df=df,
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
    )


@asset(
    name="station_module_scd",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_station_module_scd(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "station_module_scd"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            raise Exception("Empty dataframe in bronze asset")

    use_dictionary = [
        "system_id",
        "system_listener_id",
        "team_sb_id",
        "station_module_sb_id",
        "is_deleted",
    ]

    column_encoding = {"ts": "DELTA_BINARY_PACKED", "status": "DELTA_BINARY_PACKED"}

    return SQL(
        "select * from $df",
        df=df,
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
    )


@asset(
    name="player_scd",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def bronze_player_scd(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "player_scd"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            if not is_playerless_system(context.partition_key, connection):
                raise Exception("Empty dataframe in bronze asset")

    use_dictionary = [
        "system_id",
        "system_listener_id",
        "player_sb_id",
        "ship_id",
        "is_alive",
        "is_deleted",
    ]

    column_encoding = {
        "ts": "DELTA_BINARY_PACKED",
        "score": "DELTA_BINARY_PACKED",
        "x": "DELTA_BINARY_PACKED",
        "y": "DELTA_BINARY_PACKED",
    }

    return SQL(
        "select * from $df",
        df=df,
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
    )


@asset(
    name="alien_scd",
    key_prefix=["starblast_info", "bronze", "landing"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    code_version="1",
)
def stg_alien_scd(
    context: AssetExecutionContext, starblast_info_pg: PostgresResource
) -> SQL:
    query = get_query(["bronze", "landing", "alien_scd"]).format(
        system_id=context.partition_key
    )

    with starblast_info_pg.connect() as connection:
        create_system_system_listener_temp_table(connection=connection)

        df = pl.read_database(
            query=query,
            connection=connection,
        )

        if df.shape[0] == 0:
            raise Exception("Empty dataframe in bronze asset")

    use_dictionary = [
        "system_id",
        "system_listener_id",
        "alien_sb_id",
        "is_deleted",
    ]

    column_encoding = {
        "ts": "DELTA_BINARY_PACKED",
        "x": "DELTA_BINARY_PACKED",
        "y": "DELTA_BINARY_PACKED",
    }

    return SQL(
        "select * from $df",
        df=df,
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
    )
