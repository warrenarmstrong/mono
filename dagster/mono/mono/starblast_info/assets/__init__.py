from dagster import AssetKey, define_asset_job, load_assets_from_modules

from ..partitions import starblast_info_system_partition_def
from . import bronze, deduped, silver, util

assets = load_assets_from_modules([bronze, silver, deduped])

create_system_system_listener_temp_table = util.create_system_system_listener_temp_table

starblast_info_persist_system_job = define_asset_job(
    name="starblast_info_persist_system_job",
    selection=[
        AssetKey(["starblast_info", "bronze", "landing", "system"]),
        AssetKey(["starblast_info", "bronze", "landing", "team"]),
        AssetKey(["starblast_info", "bronze", "landing", "station_module"]),
        AssetKey(["starblast_info", "bronze", "landing", "player"]),
        AssetKey(["starblast_info", "bronze", "landing", "team_scd"]),
        AssetKey(["starblast_info", "bronze", "landing", "station_module_scd"]),
        AssetKey(["starblast_info", "bronze", "landing", "player_scd"]),
        AssetKey(["starblast_info", "bronze", "landing", "alien_scd"]),
    ],
    partitions_def=starblast_info_system_partition_def,
    tags={
        "dagster/priority": 2,
        "dagster/max_retries": 3,
        "dagster/retry_strategy": "FROM_FAILURE",
    },
)

starblast_info_materialize_silver_job = define_asset_job(
    name="starblast_info_materialize_silver_job",
    selection=[
        AssetKey(["starblast_info", "bronze", "deduped", "system"]),
        AssetKey(["starblast_info", "bronze", "deduped", "team"]),
        AssetKey(["starblast_info", "bronze", "deduped", "station_module"]),
        AssetKey(["starblast_info", "bronze", "deduped", "player"]),
        AssetKey(["starblast_info", "bronze", "deduped", "team_scd"]),
        AssetKey(["starblast_info", "bronze", "deduped", "station_module_scd"]),
        AssetKey(["starblast_info", "bronze", "deduped", "player_scd"]),
        AssetKey(["starblast_info", "bronze", "deduped", "alien_scd"]),
        AssetKey(["starblast_info", "silver", "dim_system"]),
        AssetKey(["starblast_info", "silver", "dim_team"]),
        AssetKey(["starblast_info", "silver", "dim_station_module"]),
        AssetKey(["starblast_info", "silver", "dim_player"]),
        AssetKey(["starblast_info", "silver", "dim_team_scd"]),
        AssetKey(["starblast_info", "silver", "dim_station_module_scd"]),
        AssetKey(["starblast_info", "silver", "dim_player_scd"]),
        AssetKey(["starblast_info", "silver", "dim_alien_scd"]),
    ],
    partitions_def=starblast_info_system_partition_def,
    tags={
        "dagster/priority": 1,
        "dagster/max_retries": 3,
        "dagster/retry_strategy": "ALL_STEPS",
    },
)
