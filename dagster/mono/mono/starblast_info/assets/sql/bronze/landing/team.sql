WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            t.system_listener_id::TEXT AS system_listener_id,
            t.team_sb_id,

            t.faction,
            t.base_name,
            t.hue,
            t.station_phase
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.team AS t
                ON ssl.system_listener_id = t.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    team_sb_id