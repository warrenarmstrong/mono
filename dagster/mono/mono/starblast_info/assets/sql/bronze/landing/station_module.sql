WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            sm.system_listener_id::TEXT AS system_listener_id,
            sm.team_sb_id,
            sm.station_module_sb_id,

            sm.type,
            sm.x,
            sm.y,
            sm.dir
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.station_module AS sm
                ON ssl.system_listener_id = sm.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    team_sb_id,
    station_module_sb_id