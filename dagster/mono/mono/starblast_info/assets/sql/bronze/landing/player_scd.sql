WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            p_scd.system_listener_id::TEXT AS system_listener_id,
            p_scd.player_sb_id,
            p_scd.ts,

            p_scd.score,
            p_scd.ship_id,
            p_scd.is_alive,
            p_scd.x,
            p_scd.y,
            p_scd.is_deleted
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.player_scd AS p_scd
                ON ssl.system_listener_id = p_scd.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    player_sb_id,
    ts