WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            t_scd.system_listener_id::TEXT AS system_listener_id,
            t_scd.team_sb_id,
            t_scd.ts,

            t_scd.is_open,
            t_scd.base_level,
            t_scd.crystals,
            t_scd.is_deleted
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.team_scd AS t_scd
                ON ssl.system_listener_id = t_scd.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    team_sb_id,
    ts