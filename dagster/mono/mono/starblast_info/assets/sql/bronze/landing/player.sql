WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            p.system_listener_id::TEXT AS system_listener_id,
            p.player_sb_id,
            p.team_sb_id,

            p.name,
            p.hue,
            p.custom_badge,
            p.custom_finish,
            p.custom_laser,
            p.custom_hue
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.player AS p
                ON ssl.system_listener_id = p.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    player_sb_id