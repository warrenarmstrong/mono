WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            a_scd.system_listener_id::TEXT AS system_listener_id,
            a_scd.alien_sb_id,
            a_scd.ts,
            
            a_scd.x,
            a_scd.y,

            is_deleted
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.alien_scd AS a_scd
                ON ssl.system_listener_id = a_scd.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    alien_sb_id,
    ts