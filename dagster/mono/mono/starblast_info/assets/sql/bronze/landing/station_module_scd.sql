WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,

            sm_scd.system_listener_id::TEXT AS system_listener_id,
            sm_scd.team_sb_id,
            sm_scd.station_module_sb_id,
            sm_scd.ts,

            status,
            is_deleted
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.station_module_scd AS sm_scd
                ON ssl.system_listener_id = sm_scd.system_listener_id
    )
SELECT *
FROM result
ORDER BY
    system_listener_id,
    team_sb_id,
    station_module_sb_id,
    ts