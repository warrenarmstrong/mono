WITH
    partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
    result AS (
        SELECT
            ssl.system_id::TEXT AS system_id,
            ssl.system_listener_priority,

            s.system_listener_id::TEXT AS system_listener_id,
			s.system_sb_id,
			s.server_address,
			s.welcome_ts,
			s.listener_version,
			s.version,
			s.seed,
			s.servertime,
			s.name,
			s.size,
			s.region,
			s.mode,
			s.max_players,
			s.crystal_value,
			s.crystal_drop,
			s.map_size,
			s.lives,
			s.max_level,
			s.friendly_colors,
			s.close_time,
			s.close_number,
			s.unlisted,
			s.survival_time,
			s.survival_level,
			s.starting_ship,
			s.starting_ship_maxed,
			s.asteroids_strength,
			s.friction_ratio,
			s.speed_mod,
			s.rcs_toggle,
			s.weapon_drop,
			s.mines_self_destroy,
			s.mines_destroy_delay,
			s.healing_enabled,
			s.healing_ratio,
			s.shield_regen_factor,
			s.power_regen_factor,
			s.auto_refill,
			s.projectile_speed,
			s.strafe,
			s.release_crystal,
			s.large_grid,
			s.bouncing_lasers,
			s.max_tier_lives,
			s.auto_assign_teams,
			s.station_size,
			s.crystal_capacity,
			s.deposit_shield,
			s.spawning_shield,
			s.structure_shield,
			s.deposit_regen,
			s.spawning_regen,
			s.structure_regen,
			s.repair_threshold,
			s.all_ships_can_dock,
			s.all_ships_can_respawn,
            s.latest_ts
        FROM
            partition_ssl AS ssl
            INNER JOIN listener.system AS s
                ON ssl.system_listener_id = s.system_listener_id
    )
SELECT *
FROM result
ORDER BY system_listener_id