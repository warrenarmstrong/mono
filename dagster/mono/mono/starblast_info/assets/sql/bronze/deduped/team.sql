WITH
    team_duped AS (
        SELECT
            COLUMNS(t.* EXCLUDE (system_listener_id)),
            ROW_NUMBER() OVER (
                PARTITION BY t.team_sb_id
                ORDER BY s.system_listener_priority
            ) AS row_num
        FROM
            $bronze_landing_team AS t
            INNER JOIN $bronze_landing_system AS s
                ON t.system_listener_id = s.system_listener_id
    ),
    team_deduped AS (
        SELECT * EXCLUDE (row_num)
        FROM team_duped
        WHERE row_num = 1
    )
SELECT *
FROM team_deduped
ORDER BY
    system_id,
    team_sb_id