WITH
    station_module_duped AS (
        SELECT
            COLUMNS(sm.* EXCLUDE (system_listener_id)),
            ROW_NUMBER() OVER (
                PARTITION BY sm.team_sb_id, sm.station_module_sb_id
                ORDER BY s.system_listener_priority
            ) AS row_num
        FROM
            $bronze_landing_station_module AS sm
            INNER JOIN $bronze_landing_system AS s
                ON sm.system_listener_id = s.system_listener_id
    ),
    station_module_deduped AS (
        SELECT * EXCLUDE (row_num)
        FROM station_module_duped
        WHERE row_num = 1
    )
SELECT *
FROM station_module_deduped
ORDER BY
    system_id,
    team_sb_id,
    station_module_sb_id