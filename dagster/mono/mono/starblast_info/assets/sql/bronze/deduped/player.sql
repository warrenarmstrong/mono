WITH
    player_duped AS (
        SELECT
            COLUMNS(p.* EXCLUDE (system_listener_id)),
            ROW_NUMBER() OVER (
                PARTITION BY p.player_sb_id
                ORDER BY s.system_listener_priority
            ) AS row_num
        FROM
            $bronze_landing_player AS p
            INNER JOIN $bronze_landing_system AS s
                ON p.system_listener_id = s.system_listener_id
    ),
    player_deduped AS (
        SELECT * EXCLUDE (row_num)
        FROM player_duped
        WHERE row_num = 1
    )
SELECT *
FROM player_deduped
ORDER BY
    system_id,
    player_sb_id