SELECT COLUMNS(* EXCLUDE(system_listener_id, system_listener_priority))
FROM $bronze_landing_system
WHERE system_listener_priority = 1
ORDER BY system_id