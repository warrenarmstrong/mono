WITH
    station_module_scd_enriched AS (
        SELECT
            sm_scd.system_id,
            sm_scd.team_sb_id,
            sm_scd.station_module_sb_id,
        
            sm_scd.ts - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS from_system_second,
            COALESCE((LEAD(sm_scd.ts) OVER (PARTITION BY sm_scd.team_sb_id, sm_scd.station_module_sb_id ORDER BY sm_scd.ts) - 1), s.latest_ts) - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS to_system_second,
        
            TO_TIMESTAMP(sm_scd.ts) AS from_ts,
            TO_TIMESTAMP(COALESCE((LEAD(sm_scd.ts) OVER (PARTITION BY sm_scd.team_sb_id, sm_scd.station_module_sb_id ORDER BY sm_scd.ts) - 1), s.latest_ts)) AS to_ts,
        
            sm_scd.status,
            sm_scd.is_deleted
        FROM
            $bronze_landing_station_module_scd AS sm_scd
            INNER JOIN $bronze_landing_system AS s
                ON s.system_id = sm_scd.system_id
        WHERE s.system_listener_priority = 1
        ORDER BY
            sm_scd.system_id,
            sm_scd.team_sb_id,
            sm_scd.station_module_sb_id,
            sm_scd.ts
    )
SELECT COLUMNS(* EXCLUDE(is_deleted))
FROM station_module_scd_enriched
WHERE NOT is_deleted
ORDER BY
    system_id,
    team_sb_id,
    station_module_sb_id,
    from_system_second