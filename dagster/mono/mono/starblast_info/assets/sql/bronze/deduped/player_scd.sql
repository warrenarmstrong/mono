WITH
    player_scd_enriched AS (
        SELECT
            p_scd.system_id,
            p_scd.player_sb_id,
        
            p_scd.ts - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS from_system_second,
            COALESCE((LEAD(p_scd.ts) OVER (PARTITION BY p_scd.player_sb_id ORDER BY p_scd.ts) - 1), s.latest_ts) - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS to_system_second,
        
            TO_TIMESTAMP(p_scd.ts) AS from_ts,
            TO_TIMESTAMP(COALESCE((LEAD(p_scd.ts) OVER (PARTITION BY p_scd.player_sb_id ORDER BY p_scd.ts) - 1), s.latest_ts)) AS to_ts,
        
            p_scd.score,
            p_scd.ship_id,
            p_scd.is_alive,
            p_scd.x,
            p_scd.y,
            p_scd.is_deleted
        FROM
            $bronze_landing_player_scd AS p_scd
            INNER JOIN $bronze_landing_system AS s
                ON s.system_id = p_scd.system_id
        WHERE TRUE
            AND s.system_listener_priority = 1
        ORDER BY
            p_scd.system_id,
            p_scd.player_sb_id,
            p_scd.ts
    )
SELECT COLUMNS(* EXCLUDE(is_deleted))
FROM player_scd_enriched
WHERE NOT is_deleted
ORDER BY
    system_id,
    player_sb_id,
    from_system_second