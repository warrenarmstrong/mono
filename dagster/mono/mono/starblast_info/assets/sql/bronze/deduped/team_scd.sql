WITH
    bronze_team_scd_duped AS (
        SELECT
            *,
            ROW_NUMBER() OVER (PARTITION BY system_id, team_sb_id, ts) AS row_num
        FROM $bronze_landing_team_scd
    ),
    team_scd_enriched AS (
        SELECT
            t_scd.system_id,
            t_scd.team_sb_id,
        
            t_scd.ts - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS from_system_second,
            COALESCE((LEAD(t_scd.ts) OVER (PARTITION BY t_scd.team_sb_id ORDER BY t_scd.ts) - 1), s.latest_ts) - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS to_system_second,
        
            TO_TIMESTAMP(t_scd.ts) AS from_ts,
            TO_TIMESTAMP(COALESCE((LEAD(t_scd.ts) OVER (PARTITION BY t_scd.team_sb_id ORDER BY t_scd.ts) - 1), s.latest_ts)) AS to_ts,
        
            t_scd.is_open,
            t_scd.base_level,
            t_scd.crystals,
            t_scd.is_deleted
        FROM
            bronze_team_scd_duped AS t_scd
            INNER JOIN $bronze_landing_system AS s
                ON s.system_id = t_scd.system_id
        WHERE TRUE
            AND s.system_listener_priority = 1
            AND t_scd.row_num = 1
        ORDER BY
            t_scd.system_id,
            t_scd.team_sb_id,
            t_scd.ts
    )
SELECT COLUMNS(* EXCLUDE(is_deleted))
FROM team_scd_enriched
WHERE NOT is_deleted
ORDER BY
    system_id,
    team_sb_id,
    from_system_second