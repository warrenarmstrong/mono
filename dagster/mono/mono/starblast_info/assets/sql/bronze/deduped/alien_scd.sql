WITH
    alien_scd_enriched AS (
        SELECT
            a_scd.system_id,
            a_scd.alien_sb_id,
        
            a_scd.ts - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS from_system_second,
            COALESCE((LEAD(a_scd.ts) OVER (PARTITION BY a_scd.alien_sb_id ORDER BY a_scd.ts) - 1), s.latest_ts) - (((s.welcome_ts * 1000) - s.servertime) / 1000)::INT AS to_system_second,
        
            TO_TIMESTAMP(a_scd.ts) AS from_ts,
            TO_TIMESTAMP(COALESCE((LEAD(a_scd.ts) OVER (PARTITION BY a_scd.alien_sb_id ORDER BY a_scd.ts) - 1), s.latest_ts)) AS to_ts,
        
            a_scd.x,
            a_scd.y,
            a_scd.is_deleted
        FROM
            $bronze_landing_alien_scd AS a_scd
            INNER JOIN $bronze_landing_system AS s
                ON s.system_id = a_scd.system_id
        WHERE TRUE
            AND s.system_listener_priority = 1
        ORDER BY
            a_scd.system_id,
            a_scd.alien_sb_id,
            a_scd.ts
    )
SELECT COLUMNS(* EXCLUDE(is_deleted))
FROM alien_scd_enriched
WHERE NOT is_deleted
ORDER BY
    system_id,
    alien_sb_id,
    from_system_second