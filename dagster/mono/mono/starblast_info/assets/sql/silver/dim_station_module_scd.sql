WITH
    dim_base_level_bronze AS (
        SELECT
            system_id,
            team_sb_id,
            base_level,
            MIN(from_system_second) AS from_system_second,
            MAX(to_system_second) AS to_system_second,
            MIN(from_ts) AS from_ts,
            MAX(to_ts) AS to_ts
        FROM $bronze_deduped_team_scd
        GROUP BY 1, 2, 3
        ORDER BY 1, 2, 3
    ),
    dim_station_module_scd_enriched AS (
        SELECT
            sm_scd.system_id,
            sm_scd.team_sb_id,
            sm_scd.station_module_sb_id,
            
            GREATEST(sm_scd.from_system_second, t_scd.from_system_second) AS from_system_second,
            LEAST(sm_scd.to_system_second, t_scd.to_system_second) AS to_system_second,
            GREATEST(sm_scd.from_ts, t_scd.from_ts) AS from_ts,
            LEAST(sm_scd.to_ts, t_scd.to_ts) AS to_ts,
            
            sm_scd.status > 0 AS is_alive,
            CASE
                WHEN sm.type LIKE 'd_' THEN (s.deposit_shield[t_scd.base_level + 1] * (GREATEST(0, sm_scd.status - 1) / 254))::INT
                WHEN sm.type LIKE 'sp_' THEN (s.spawning_shield[t_scd.base_level + 1] * (GREATEST(0, sm_scd.status - 1) / 254))::INT
                WHEN sm.type LIKE 'st_' THEN (s.structure_shield[t_scd.base_level + 1] * (GREATEST(0, sm_scd.status - 1) / 254))::INT
                ELSE 'ERROR'
            END AS shield,
            CASE
                WHEN sm.type LIKE 'd_' THEN s.deposit_shield[t_scd.base_level + 1]
                WHEN sm.type LIKE 'sp_' THEN s.spawning_shield[t_scd.base_level + 1]
                WHEN sm.type LIKE 'st_' THEN s.structure_shield[t_scd.base_level + 1]
                ELSE 'ERROR'
            END AS full_shield,
            sm_scd.status
        FROM
            $bronze_deduped_station_module_scd AS sm_scd
            INNER JOIN $bronze_deduped_system AS s
                ON sm_scd.system_id = s.system_id
            INNER JOIN $bronze_deduped_station_module AS sm
                ON sm_scd.system_id = sm.system_id
                AND sm_scd.team_sb_id = sm.team_sb_id
                AND sm_scd.station_module_sb_id = sm.station_module_sb_id
            INNER JOIN dim_base_level_bronze AS t_scd
                ON sm_scd.system_id = t_scd.system_id
                AND sm_scd.team_sb_id = t_scd.team_sb_id
                AND sm_scd.from_ts <= t_scd.to_ts
                AND sm_scd.to_ts >= t_scd.from_ts
                AND GREATEST(sm_scd.from_ts, t_scd.from_ts) <= LEAST(sm_scd.to_ts, t_scd.to_ts)
        ORDER BY
            sm_scd.system_id,
            sm_scd.team_sb_id,
            sm_scd.station_module_sb_id,
            GREATEST(sm_scd.from_ts, t_scd.from_ts)
    )
SELECT *
FROM dim_station_module_scd_enriched
ORDER BY
    system_id,
    team_sb_id,
    station_module_sb_id,
    from_ts