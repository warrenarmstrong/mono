WITH
    team_result AS (
        SELECT
            sm_scd.system_id,
            sm_scd.team_sb_id,
            MAX(sm_scd.to_system_second) AS last_alive_second,
            RANK() OVER (PARTITION BY sm_scd.system_id ORDER BY MAX(sm_scd.to_system_second) DESC) AS team_rank
        FROM
            $bronze_deduped_station_module_scd AS sm_scd
            INNER JOIN $bronze_deduped_station_module AS sm
                ON sm_scd.system_id = sm.system_id
                AND sm_scd.team_sb_id = sm.team_sb_id
                AND sm_scd.station_module_sb_id = sm.station_module_sb_id
        WHERE TRUE
            AND (sm.type LIKE 'd_' OR sm.type LIKE 'sp_')
            AND sm_scd.status > 0
        GROUP BY
            sm_scd.system_id,
            sm_scd.team_sb_id
        ORDER BY
            sm_scd.system_id,
            MAX(sm_scd.to_system_second) DESC
    ),
    station_module_scd_row_numbered AS (
        SELECT
            *,
            ROW_NUMBER() OVER (PARTITION BY system_id, team_sb_id, station_module_sb_id ORDER BY from_system_second DESC) AS row_num
        FROM $bronze_deduped_station_module_scd
    ),
    end_observed AS (
        SELECT
            sm_scd.system_id,
            SUM(CASE WHEN sm_scd.status = 0 THEN 1 ELSE 0 END) = 12 AS end_observed,
        FROM
            station_module_scd_row_numbered AS sm_scd
            INNER JOIN $bronze_deduped_station_module AS sm
                ON sm_scd.system_id = sm.system_id
                AND sm_scd.team_sb_id = sm.team_sb_id
                AND sm_scd.station_module_sb_id = sm.station_module_sb_id
        WHERE TRUE
            AND sm_scd.row_num = 1
            AND (sm.type LIKE 'd_' OR sm.type LIKE 'sp_')
        GROUP BY sm_scd.system_id
        ORDER BY sm_scd.system_id
    ),
    team_result_observed AS (
        SELECT
            r.system_id,
            r.team_sb_id,
            CASE WHEN e.end_observed THEN r.last_alive_second ELSE NULL END AS last_alive_second,
            CASE WHEN e.end_observed THEN r.team_rank ELSE NULL END AS team_rank
        FROM
            team_result AS r
            CROSS JOIN end_observed AS e
    ),
    team_filled AS (
        SELECT
            t.system_id,
            t.team_sb_id,
            t.faction,
            t.base_name,
            t.hue,
            t.station_phase,
        
            tr.last_alive_second,
            tr.team_rank AS rank
        FROM
            $bronze_deduped_team AS t
            INNER JOIN team_result_observed AS tr
                ON t.system_id = tr.system_id
                AND t.team_sb_id = tr.team_sb_id
        ORDER BY
            t.system_id,
            t.team_sb_id
    )
SELECT *
FROM team_filled
ORDER BY
    system_id,
    team_sb_id