SELECT
    COLUMNS(t_scd.* EXCLUDE(crystals)),
    t_scd.crystals AS level_crystals,
    t_scd.crystals + COALESCE(s.crystal_capacity[t_scd.base_level], 0) AS total_crystals,
    s.crystal_capacity[t_scd.base_level + 1] AS full_crystals
FROM
    $bronze_deduped_team_scd AS t_scd
    INNER JOIN $bronze_deduped_system AS s
        ON t_scd.system_id = s.system_id
ORDER BY
    system_id,
    team_sb_id,
    from_system_second