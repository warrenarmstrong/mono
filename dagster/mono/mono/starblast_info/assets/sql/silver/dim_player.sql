SELECT
    *,
    custom_badge IS NOT NULL AS has_ecp,
FROM $bronze_deduped_player
ORDER BY
    system_id,
    player_sb_id