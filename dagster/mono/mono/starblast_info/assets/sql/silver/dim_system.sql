SELECT
    system_id,
    system_sb_id,
    SPLIT_PART(server_address, ':', 1) AS ip_address,
    SPLIT_PART(server_address, ':', 2) AS port,
    TO_TIMESTAMP(welcome_ts) AS welcome_message_ts,

    COLUMNS(* EXCLUDE(system_id, system_sb_id, server_address, welcome_ts))
FROM $bronze_deduped_system
ORDER BY system_id