SELECT
    *,
    CASE
        WHEN type LIKE 'd_' THEN 'deposit'
        WHEN type LIKE 'sp_' THEN 'spawning'
        WHEN type LIKE 'st_' THEN 'structure'
        ELSE 'ERROR'
    END AS type_name,
FROM $bronze_deduped_station_module
ORDER BY
    system_id,
    team_sb_id,
    station_module_sb_id