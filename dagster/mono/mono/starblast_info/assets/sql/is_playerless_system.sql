WITH
	partition_ssl AS (
		SELECT *
		FROM system_system_listener
		WHERE system_id = '{system_id}'
	),
	system_length AS (
		SELECT MAX(latest_ts - (((welcome_ts * 1000) - servertime) / 1000)) < 200 AS condition
		FROM
			listener.system AS s
			INNER JOIN partition_ssl AS ssl
				ON s.system_listener_id = ssl.system_listener_id
	),
	system_row_count AS (
		SELECT COUNT(*) >= 1 AS condition
		FROM
			listener.system AS s
			INNER JOIN partition_ssl AS ssl
				ON s.system_listener_id = ssl.system_listener_id
	),
	team_row_count AS (
		SELECT COUNT(*) >= 3 AS condition
		FROM
			listener.team AS t
			INNER JOIN partition_ssl AS ssl
				ON t.system_listener_id = ssl.system_listener_id
	),
	station_module_row_count AS (
		SELECT COUNT(*) >= 36 AS condition
		FROM
			listener.station_module AS sm
			INNER JOIN partition_ssl AS ssl
				ON sm.system_listener_id = ssl.system_listener_id
	),
	team_scd_row_count AS (
		SELECT COUNT(*) >= 1 AS condition
		FROM
			listener.team_scd AS t_scd
			INNER JOIN partition_ssl AS ssl
				ON t_scd.system_listener_id = ssl.system_listener_id
	),
	station_module_scd_row_count AS (
		SELECT COUNT(*) >= 1 AS condition
		FROM
			listener.station_module_scd AS sm_scd
			INNER JOIN partition_ssl AS ssl
				ON sm_scd.system_listener_id = ssl.system_listener_id
	),
	alien_scd_row_count AS (
		SELECT COUNT(*) >= 1 AS condition
		FROM
			listener.alien_scd AS a_scd
			INNER JOIN partition_ssl AS ssl
				ON a_scd.system_listener_id = ssl.system_listener_id
	),
	all_conditions AS (
		SELECT
			sl.condition
			AND src.condition
			AND trc.condition
			AND smrc.condition
			AND tscdrc.condition
			AND smscdrc.condition
			AND ascdrc.condition AS condition
		FROM
			system_length AS sl
			CROSS JOIN system_row_count AS src
			CROSS JOIN team_row_count AS trc
			CROSS JOIN station_module_row_count AS smrc
			CROSS JOIN team_scd_row_count AS tscdrc
			CROSS JOIN station_module_scd_row_count AS smscdrc
			CROSS JOIN alien_scd_row_count AS ascdrc
	)
SELECT *
FROM all_conditions