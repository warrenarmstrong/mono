CREATE TEMP TABLE system_system_listener AS (
	WITH
		add_start_ts AS (
			SELECT
				system_listener_id,
				system_sb_id,
				welcome_ts,
				servertime,
				latest_ts,
				(welcome_ts * 1000) - servertime as start_ts
			FROM listener.system
		),
		add_start_ts_diff AS (
			SELECT
				*,
				start_ts - LAG(start_ts) OVER (
					PARTITION BY system_sb_id
					ORDER BY start_ts, welcome_ts, system_listener_id
				)  AS start_ts_diff
			FROM add_start_ts
		),
		add_system_listener_group_id AS (
			SELECT
				*,
				SUM(CASE WHEN (start_ts_diff IS NULL) OR (start_ts_diff > 5000) THEN 1 ELSE 0 END) OVER (
					ORDER BY system_sb_id, start_ts, welcome_ts, system_listener_id
				) AS system_listener_group_id
			FROM add_start_ts_diff
		),
		system_system_listener AS (
			SELECT
				FIRST_VALUE(system_listener_id) OVER (
					PARTITION BY system_listener_group_id
					ORDER BY welcome_ts, system_listener_id
				) AS system_id,
				system_listener_id,
				ROW_NUMBER() OVER (
					PARTITION BY system_listener_group_id
					ORDER BY welcome_ts, system_listener_id
				) AS system_listener_priority

			FROM add_system_listener_group_id
			ORDER BY
				system_id,
				system_listener_priority
		)
	SELECT * FROM system_system_listener
);