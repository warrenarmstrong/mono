from pathlib import Path

from sqlalchemy.engine.base import Connection
from sqlalchemy.sql import text


def get_query(file_path: list[str]) -> str:
    path = Path(__file__).parent / "sql" / f"{'/'.join(file_path)}.sql"

    with open(path, "r") as f:
        query = f.read()

    return query


def create_system_system_listener_temp_table(connection: Connection) -> None:
    query = get_query(["system_system_listener"])
    connection.execute(text(query))
