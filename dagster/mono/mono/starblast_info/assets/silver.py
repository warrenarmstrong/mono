import polars as pl

from dagster import AssetExecutionContext, AssetIn, AssetKey, asset

from ...shared.resources import SQL  # type: ignore
from ..partitions import starblast_info_system_partition_def
from .util import get_query


@asset(
    name="dim_system",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "system"])
        )
    },
    code_version="1",
)
def silver_dim_system(
    context: AssetExecutionContext, bronze_deduped_system: SQL
) -> SQL:
    return SQL(
        get_query(["silver", "dim_system"]), bronze_deduped_system=bronze_deduped_system
    )


@asset(
    name="dim_team",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_team": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "team"])
        ),
        "bronze_deduped_station_module_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "station_module_scd"])
        ),
        "bronze_deduped_station_module": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "station_module"])
        ),
    },
    code_version="2",
)
def silver_dim_team(
    context: AssetExecutionContext,
    bronze_deduped_team: SQL,
    bronze_deduped_station_module_scd: SQL,
    bronze_deduped_station_module: SQL,
) -> SQL:
    return SQL(
        get_query(["silver", "dim_team"]),
        bronze_deduped_team=bronze_deduped_team,
        bronze_deduped_station_module_scd=bronze_deduped_station_module_scd,
        bronze_deduped_station_module=bronze_deduped_station_module,
    )


@asset(
    name="dim_station_module",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_station_module": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "station_module"])
        ),
    },
    code_version="3",
)
def silver_dim_station_module(
    context: AssetExecutionContext,
    bronze_deduped_station_module: SQL,
) -> SQL:
    return SQL(
        get_query(["silver", "dim_station_module"]),
        bronze_deduped_station_module=bronze_deduped_station_module,
    )


@asset(
    name="dim_player",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_player": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "player"])
        ),
    },
    code_version="2",
)
def silver_dim_player(
    context: AssetExecutionContext,
    bronze_deduped_player: SQL,
) -> SQL:
    return SQL(
        get_query(["silver", "dim_player"]), bronze_deduped_player=bronze_deduped_player
    )


@asset(
    name="dim_team_scd",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "system"])
        ),
        "bronze_deduped_team_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "team_scd"])
        ),
    },
    code_version="4",
)
def silver_dim_team_scd(
    context: AssetExecutionContext,
    bronze_deduped_system: SQL,
    bronze_deduped_team_scd: SQL,
) -> SQL:
    use_dictionary = [
        "system_id",
        "team_sb_id",
        "is_open",
        "base_level",
        "full_crystals",
    ]

    column_encoding = {
        "from_system_second": "DELTA_BINARY_PACKED",
        "to_system_second": "DELTA_BINARY_PACKED",
        "from_ts": "DELTA_BINARY_PACKED",
        "to_ts": "DELTA_BINARY_PACKED",
        "level_crystals": "DELTA_BINARY_PACKED",
        "total_crystals": "DELTA_BINARY_PACKED",
    }

    return SQL(
        get_query(["silver", "dim_team_scd"]),
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
        bronze_deduped_system=bronze_deduped_system,
        bronze_deduped_team_scd=bronze_deduped_team_scd,
    )


@asset(
    name="dim_station_module_scd",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_system": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "system"])
        ),
        "bronze_deduped_station_module_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "station_module_scd"])
        ),
        "bronze_deduped_station_module": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "station_module"])
        ),
        "bronze_deduped_team_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "team_scd"])
        ),
    },
    code_version="4",
)
def silver_dim_station_module_scd(
    context: AssetExecutionContext,
    bronze_deduped_system: SQL,
    bronze_deduped_station_module_scd: SQL,
    bronze_deduped_station_module: SQL,
    bronze_deduped_team_scd: SQL,
) -> SQL:
    use_dictionary = [
        "system_id",
        "team_sb_id",
        "station_module_sb_id",
        "is_alive",
        "full_shield",
    ]

    column_encoding = {
        "from_system_second": "DELTA_BINARY_PACKED",
        "to_system_second": "DELTA_BINARY_PACKED",
        "from_ts": "DELTA_BINARY_PACKED",
        "to_ts": "DELTA_BINARY_PACKED",
        "shield": "DELTA_BINARY_PACKED",
        "status": "DELTA_BINARY_PACKED",
    }

    return SQL(
        get_query(["silver", "dim_station_module_scd"]),
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
        bronze_deduped_system=bronze_deduped_system,
        bronze_deduped_station_module_scd=bronze_deduped_station_module_scd,
        bronze_deduped_station_module=bronze_deduped_station_module,
        bronze_deduped_team_scd=bronze_deduped_team_scd,
    )


@asset(
    name="dim_player_scd",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_player_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "player_scd"])
        ),
    },
    code_version="1",
)
def silver_dim_player_scd(
    context: AssetExecutionContext,
    bronze_deduped_player_scd: SQL,
) -> SQL:
    use_dictionary = [
        "system_id",
        "player_sb_id",
        "ship_id",
        "is_alive",
    ]

    column_encoding = {
        "from_system_second": "DELTA_BINARY_PACKED",
        "to_system_second": "DELTA_BINARY_PACKED",
        "from_ts": "DELTA_BINARY_PACKED",
        "to_ts": "DELTA_BINARY_PACKED",
        "score": "DELTA_BINARY_PACKED",
        "x": "DELTA_BINARY_PACKED",
        "y": "DELTA_BINARY_PACKED",
    }

    return SQL(
        get_query(["silver", "dim_player_scd"]),
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
        bronze_deduped_player_scd=bronze_deduped_player_scd,
    )


@asset(
    name="dim_alien_scd",
    key_prefix=["starblast_info", "silver"],
    partitions_def=starblast_info_system_partition_def,
    io_manager_key="starblast_info_duckpond_io_manager",
    metadata={"partition_name": "system_id"},
    ins={
        "bronze_deduped_alien_scd": AssetIn(
            key=AssetKey(["starblast_info", "bronze", "deduped", "alien_scd"])
        ),
    },
    code_version="1",
)
def silver_dim_alien_scd(
    context: AssetExecutionContext,
    bronze_deduped_alien_scd: SQL,
) -> SQL:
    use_dictionary = [
        "system_id",
        "alien_sb_id",
    ]

    column_encoding = {
        "from_system_second": "DELTA_BINARY_PACKED",
        "to_system_second": "DELTA_BINARY_PACKED",
        "from_ts": "DELTA_BINARY_PACKED",
        "to_ts": "DELTA_BINARY_PACKED",
        "x": "DELTA_BINARY_PACKED",
        "y": "DELTA_BINARY_PACKED",
    }

    return SQL(
        get_query(["silver", "dim_alien_scd"]),
        use_dictionary=use_dictionary,
        column_encoding=column_encoding,
        bronze_deduped_alien_scd=bronze_deduped_alien_scd,
    )
