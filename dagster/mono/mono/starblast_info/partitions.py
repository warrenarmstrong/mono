from dagster import DynamicPartitionsDefinition, StaticPartitionsDefinition

starblast_info_system_partition_def = DynamicPartitionsDefinition(
    name="starblast_info_system"
)
