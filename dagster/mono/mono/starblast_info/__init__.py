from dagster import Definitions, EnvVar, load_assets_from_modules
from dagster_docker import docker_executor  # type: ignore

from ..shared.io_managers import DuckPondR2IOManager  # type: ignore
from ..shared.resources import DuckDBR2Resource  # type: ignore
from ..shared.resources import PostgresResource
from . import assets
from .sensors import (starblast_info_bronze_landing_asset_sensor,
                      starblast_info_system_sensor)

all_assets = load_assets_from_modules([assets])

defs = Definitions(
    assets=all_assets,
    jobs=[assets.starblast_info_persist_system_job],
    # executor=docker_executor,
    sensors=[
        starblast_info_system_sensor,
        starblast_info_bronze_landing_asset_sensor,
    ],
    resources={
        "starblast_info_pg": PostgresResource(
            user=EnvVar("STARBLAST_INFO_PG_USER"),
            password=EnvVar("STARBLAST_INFO_PG_PASSWORD"),
            hostname="starblast-info-pg",
            db="starblast_info",
        ),
        # "starblast_info_duckdb_r2": DuckDBR2Resource(
        #     account_id=EnvVar("R2_ACCOUNT_ID"),
        #     access_key_id=EnvVar("R2_ACCESS_KEY_ID"),
        #     secret_access_key=EnvVar("R2_SECRET_ACCESS_KEY"),
        #     bucket_name=EnvVar("STARBLAST_R2_BUCKET_NAME"),
        # ),
        "starblast_info_duckpond_io_manager": DuckPondR2IOManager(
            duckdb=DuckDBR2Resource(
                account_id=EnvVar("STARBLAST_INFO_R2_ACCOUNT_ID"),
                access_key_id=EnvVar("STARBLAST_INFO_R2_ACCESS_KEY_ID"),
                secret_access_key=EnvVar("STARBLAST_INFO_R2_SECRET_ACCESS_KEY"),
                bucket_name=EnvVar("STARBLAST_INFO_R2_BUCKET_NAME"),
            )
        ),
    },
)
