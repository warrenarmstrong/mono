import io

import boto3  # type: ignore
import polars as pl
import pyarrow.parquet as pq  # type: ignore
from dagster import (ConfigurableIOManager, InputContext, OutputContext,
                     ResourceDependency)

from .resources import SQL, DuckDBR2Resource  # type: ignore


class DuckPondR2IOManager(ConfigurableIOManager):  # type: ignore
    duckdb: ResourceDependency[DuckDBR2Resource]

    def handle_output(self, context: OutputContext, sql: SQL) -> None:
        s3 = boto3.client(
            "s3",
            endpoint_url=f"https://{self.duckdb.account_id}.r2.cloudflarestorage.com",
            aws_access_key_id=self.duckdb.access_key_id,
            aws_secret_access_key=self.duckdb.secret_access_key,
        )

        if sql is None:
            return

        if not isinstance(sql, SQL):
            raise ValueError(f"Expected asset to return a SQL; got {sql!r}")

        table = self.duckdb.query(sql=sql.sql, **sql.bindings).to_arrow()
        file_content = io.BytesIO()
        pq.write_table(
            table=table,
            where=file_content,
            use_dictionary=sql.use_dictionary,
            compression=sql.compression,
            compression_level=sql.compression_level,
            column_encoding=sql.column_encoding,
            sorting_columns=sql.sorting_columns,
        )
        file_content.seek(0)
        s3.upload_fileobj(
            file_content, self.duckdb.bucket_name, self._get_path(context)
        )

    def load_input(self, context: InputContext) -> pl.DataFrame:
        uri = self._get_uri(context)
        return self.duckdb.query(f"""SELECT * FROM "{uri}" """)  # type: ignore

    def _get_path(self, context: InputContext | OutputContext) -> str:
        if not context.has_asset_key:
            raise ValueError(f"Expected asset")

        id = context.get_asset_identifier()
        partition = ""

        if context.has_partition_key:
            partition_name = context.upstream_output.metadata.get("partition_name") if context.metadata.get("partition_name") is None else context.metadata.get("partition_name")  # type: ignore
            id = id[:-1]
            partition = f"/{partition_name}={context.asset_partition_key}"

        return f"{'/'.join(id)}{partition}/data.parquet"

    def _get_uri(self, context: InputContext | OutputContext) -> str:
        return f"r2://{self.duckdb.bucket_name}/{self._get_path(context)}"
