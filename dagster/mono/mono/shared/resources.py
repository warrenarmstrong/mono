import time
from string import Template
from typing import Any

import polars as pl
import pyarrow.parquet as pq  # type: ignore
from duckdb import DuckDBPyConnection, connect
from pydantic import Field, PrivateAttr
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Connection, Engine
from sqlescapy import sqlescape  # type: ignore

from dagster import ConfigurableResource, InitResourceContext, get_dagster_logger


class PostgresResource(ConfigurableResource):  # type: ignore
    user: str
    password: str
    hostname: str
    port: str = Field(default="5432")
    db: str

    _engine: Engine = PrivateAttr()

    def setup_for_execution(self, context: InitResourceContext) -> None:
        uri = f"postgresql+psycopg2://{self.user}:{self.password}@{self.hostname}:{self.port}/{self.db}"
        self._engine = create_engine(uri)

    def teardown_after_execution(self, context: InitResourceContext) -> None:
        self._engine.dispose()

    def connect(self) -> Connection:
        return self._engine.connect()


class SQL:
    sql: str
    bindings: dict[str, Any]

    use_dictionary: bool | list[str]
    compression: str | dict[str, str]
    compression_level: int | dict[str, int] | None
    column_encoding: str | dict[str, str] | None
    sorting_columns: list[pq.SortingColumn] | None

    def __init__(
        self,
        sql: str,
        use_dictionary: bool | list[str] = True,
        compression: str | dict[str, str] = "ZSTD",
        compression_level: int | dict[str, int] | None = 22,
        column_encoding: str | dict[str, str] | None = None,
        sorting_columns: list[pq.SortingColumn] | None = None,
        **bindings: Any,
    ) -> None:
        self.sql = sql
        self.bindings = bindings

        self.use_dictionary = use_dictionary
        self.compression = compression
        self.compression_level = compression_level
        self.column_encoding = column_encoding
        self.sorting_columns = sorting_columns

    @property
    def sql_replaced(self) -> str:
        replacements = {}
        for key, value in self.bindings.items():
            if isinstance(value, pl.DataFrame):
                replacements[key] = f"df_{id(value)}"
            elif isinstance(value, SQL):
                replacements[key] = f"({value.sql_replaced})"
            elif isinstance(value, str):
                replacements[key] = f"'{sqlescape(value)}'"
            elif isinstance(value, (int, float, bool)):
                replacements[key] = str(value)
            elif value is None:
                replacements[key] = "null"
            else:
                raise ValueError(f"Invalid type for {key}")
        return Template(self.sql).safe_substitute(replacements)

    @property
    def dataframes(self) -> dict[str, pl.DataFrame]:
        dataframes = {}
        for key, value in self.bindings.items():
            if isinstance(value, pl.DataFrame):
                dataframes[f"df_{id(value)}"] = value
            elif isinstance(value, SQL):
                dataframes.update(value.dataframes)
        return dataframes


class DuckDBResource(ConfigurableResource):  # type: ignore
    setup_options: str = Field(default="")

    _connection: DuckDBPyConnection = PrivateAttr()

    def setup_for_execution(self, context: InitResourceContext) -> None:
        db = connect(":memory:")
        db.query(self.setup_options)
        self._connection = db

    def teardown_after_execution(self, context: InitResourceContext) -> None:
        self._connection.close()

    def query(self, sql: str, **bindings: Any) -> pl.DataFrame:
        query = SQL(sql=sql, **bindings)

        logger = get_dagster_logger()

        for key, value in query.dataframes.items():
            self._connection.register(key, value)

        logger.info(query.sql_replaced)

        result = self._connection.query(query.sql_replaced)

        if result is None:
            return pl.DataFrame()

        return pl.DataFrame(pl.from_arrow(result.arrow()))


class DuckDBR2Resource(ConfigurableResource):  # type: ignore
    account_id: str
    access_key_id: str
    secret_access_key: str
    bucket_name: str

    _duckdb_resource: DuckDBResource = PrivateAttr()

    def setup_for_execution(self, context: InitResourceContext) -> None:
        retry_attempts = 3
        for attempt in range(retry_attempts):
            try:
                self._duckdb_resource = DuckDBResource(
                    setup_options=f"""
                    install httpfs;
                    load httpfs;
                    CREATE SECRET secret5 (
                        TYPE R2,
                        KEY_ID '{self.access_key_id}',
                        SECRET '{self.secret_access_key}',
                        ACCOUNT_ID '{self.account_id}'
                    );
                    """
                )
                self._duckdb_resource.setup_for_execution(context)
                break
            except Exception as e:
                if attempt < retry_attempts - 1:
                    time.sleep(5)  # Wait before retrying
                else:
                    raise e  # Rethrow the exception if max retries reached

    def teardown_after_execution(self, context: InitResourceContext) -> None:
        self._duckdb_resource.teardown_after_execution(context)

    def query(self, sql: str, **bindings: Any) -> pl.DataFrame:
        new_sql = sql.replace("$bucket_prefix", f"r2://{self.bucket_name}")
        return self._duckdb_resource.query(sql=new_sql, **bindings)
