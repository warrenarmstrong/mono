from dagster import Definitions, OpExecutionContext, job, op


@op
def test_op(context: OpExecutionContext) -> None:
    context.log.info("foo")


@job
def test_job() -> None:
    test_op()


defs = Definitions(
    jobs=[test_job],
)
