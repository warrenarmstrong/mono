-- Add migration script here
ALTER TABLE listener.system
ALTER COLUMN asteroids_strength TYPE DOUBLE PRECISION
USING asteroids_strength::FLOAT