-- Add migration script here
CREATE SCHEMA listener;

CREATE TABLE listener.system (
    system_listener_id UUID NOT NULL,
    system_sb_id SMALLINT NOT NULL,
    server_address TEXT NOT NULL,
    welcome_ts BIGINT NOT NULL,
    listener_version SMALLINT NOT NULL, --maybe remove
    version SMALLINT NOT NULL,
    seed SMALLINT NOT NULL,
    servertime INT NOT NULL,
    name TEXT NOT NULL,
    size INT NOT NULL,
    region TEXT NOT NULL,
    mode TEXT NOT NULL,
    max_players INT NOT NULL,
    crystal_value DOUBLE PRECISION NOT NULL,
    crystal_drop INT NOT NULL,
    map_size INT NOT NULL,
    lives INT NOT NULL,
    max_level INT NOT NULL,
    friendly_colors INT NOT NULL,
    close_time INT NOT NULL,
    close_number INT NOT NULL,
    unlisted BOOLEAN NOT NULL,
    survival_time INT NOT NULL,
    survival_level INT NOT NULL,
    starting_ship INT NOT NULL,
    starting_ship_maxed BOOLEAN NOT NULL,
    asteroids_strength INT NOT NULL,
    friction_ratio DOUBLE PRECISION NOT NULL,
    speed_mod DOUBLE PRECISION NOT NULL,
    rcs_toggle BOOLEAN NOT NULL,
    weapon_drop INT NOT NULL,
    mines_self_destroy BOOLEAN NOT NULL,
    mines_destroy_delay INT NOT NULL,
    healing_enabled BOOLEAN NOT NULL,
    healing_ratio DOUBLE PRECISION NOT NULL,
    shield_regen_factor DOUBLE PRECISION NOT NULL,
    power_regen_factor DOUBLE PRECISION NOT NULL,
    auto_refill BOOLEAN NOT NULL,
    projectile_speed DOUBLE PRECISION NOT NULL,
    strafe DOUBLE PRECISION NOT NULL,
    release_crystal BOOLEAN NOT NULL,
    large_grid BOOLEAN NOT NULL,
    bouncing_lasers INT NOT NULL,
    max_tier_lives INT NOT NULL,
    auto_assign_teams BOOLEAN NOT NULL,
    station_size INT NOT NULL,
    crystal_capacity INT[] NOT NULL,
    deposit_shield INT[] NOT NULL,
    spawning_shield INT[] NOT NULL,
    structure_shield INT[] NOT NULL,
    deposit_regen INT[] NOT NULL,
    spawning_regen INT[] NOT NULL,
    structure_regen INT[] NOT NULL,
    repair_threshold DOUBLE PRECISION NOT NULL,
    all_ships_can_dock BOOLEAN NOT NULL,
    all_ships_can_respawn BOOLEAN NOT NULL,
    latest_ts BIGINT NOT NULL,

    PRIMARY KEY (system_listener_id)
);

CREATE TABLE listener.team(
    system_listener_id UUID NOT NULL,
    team_sb_id SMALLINT NOT NULL,
    faction TEXT NOT NULL,
    base_name TEXT NOT NULL,
    hue INT NOT NULL,
    station_phase DOUBLE PRECISION NOT NULL,

    PRIMARY KEY (system_listener_id, team_sb_id),
    FOREIGN KEY (system_listener_id) REFERENCES listener.system(system_listener_id)
);

CREATE TABLE listener.station_module (
    system_listener_id UUID NOT NULL,
    team_sb_id SMALLINT NOT NULL,
    station_module_sb_id SMALLINT NOT NULL,
    type TEXT NOT NULL,
    x DOUBLE PRECISION NOT NULL,
    y DOUBLE PRECISION NOT NULL,
    dir SMALLINT NOT NULL,

    PRIMARY KEY (system_listener_id, team_sb_id, station_module_sb_id),
    FOREIGN KEY (system_listener_id) REFERENCES listener.system(system_listener_id),
    FOREIGN KEY (
        system_listener_id, team_sb_id
    ) REFERENCES listener.team(system_listener_id, team_sb_id)
);

CREATE TABLE listener.player (
    system_listener_id UUID NOT NULL,
    team_sb_id SMALLINT NOT NULL,
    player_sb_id SMALLINT NOT NULL,
    name TEXT NOT NULL,
    hue INT NOT NULL,
    custom_badge TEXT,
    custom_finish TEXT,
    custom_laser TEXT,
    custom_hue INT,

    PRIMARY KEY (system_listener_id, player_sb_id),
    FOREIGN KEY (system_listener_id) REFERENCES listener.system(system_listener_id),
    FOREIGN KEY (
        system_listener_id, team_sb_id
    ) REFERENCES listener.team(system_listener_id, team_sb_id)
);

CREATE TABLE listener.player_scd (
    system_listener_id UUID NOT NULL,
    player_sb_id SMALLINT NOT NULL,
    ts BIGINT NOT NULL,
    score INT NOT NULL,
    ship_id SMALLINT NOT NULL,
    is_alive BOOLEAN NOT NULL,
    x SMALLINT NOT NULL,
    y SMALLINT NOT NULL,
    is_deleted BOOLEAN NOT NULL
);

CREATE TABLE listener.alien_scd (
    system_listener_id UUID NOT NULL,
    alien_sb_id INT NOT NULL,
    ts BIGINT NOT NULL,
    x SMALLINT NOT NULL,
    y SMALLINT NOT NULL,
    is_deleted BOOLEAN NOT NULL
);

CREATE TABLE listener.team_scd (
    system_listener_id UUID NOT NULL,
    team_sb_id SMALLINT NOT NULL,
    ts BIGINT NOT NULL,
    is_open BOOLEAN NOT NULL,
    base_level SMALLINT NOT NULL,
    crystals BIGINT NOT NULL,
    is_deleted BOOLEAN NOT NULL
);

CREATE TABLE listener.station_module_scd (
    system_listener_id UUID NOT NULL,
    team_sb_id SMALLINT NOT NULL,
    station_module_sb_id SMALLINT NOT NULL,
    ts BIGINT NOT NULL,
    status SMALLINT NOT NULL,
    is_deleted BOOLEAN NOT NULL
);