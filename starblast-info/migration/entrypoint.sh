#!/bin/sh

DATABASE_URL=postgresql://$(cat /run/secrets/starblast-info-pg-user):$(cat /run/secrets/starblast-info-pg-password)@starblast-info-pg:5432/$(cat /run/secrets/starblast-info-pg-db)

export STARBLAST_INFO_PG_WEBSERVER_USER=$(cat /run/secrets/starblast-info-pg-webserver-user)
export STARBLAST_INFO_PG_WEBSERVER_PASSWORD=$(cat /run/secrets/starblast-info-pg-webserver-password)
export STARBLAST_INFO_PG_DAGSTER_USER=$(cat /run/secrets/starblast-info-pg-dagster-user)
export STARBLAST_INFO_PG_DAGSTER_PASSWORD=$(cat /run/secrets/starblast-info-pg-dagster-password)

for input_file in "migrations"/*; do
    if [ ! -f "$input_file" ]; then
        continue
    fi
    temp_file="$(mktemp)"
    envsubst < "$input_file" > "$temp_file"
    mv "$temp_file" "$input_file"
done

exec sqlx migrate run --database-url "$DATABASE_URL"