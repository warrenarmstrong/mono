use crate::schema::{
    AlienSCD,
    PlayerSCD,
    StationModuleSCD,
    TeamSCD,
    Player,
    StationModule,
    Team,
    System,
    Starblast,
    StarblastEntity
};
use std::collections::HashMap;

pub struct TypeDiff<T: StarblastEntity> {
    pub insert: Vec<T>,
    pub update: Vec<T>,
    pub delete: Vec<T>,
}

impl<T: StarblastEntity> TypeDiff<T> {
    fn new(old_map: HashMap<u64, T>, new_map: &HashMap<u64, T>) -> Self {
        let mut insert: Vec<T> = Vec::new();
        let mut update: Vec<T> = Vec::new();
        let mut delete: Vec<T> = Vec::new();

        for (new_id, new_t) in new_map {
            if !old_map.contains_key(&new_id) {
                insert.push(new_t.clone());
            } else if !(new_t == old_map.get(&new_id).unwrap()) {
                update.push(new_t.clone())
            }
        }

        for (old_id, old_t) in old_map {
            if !new_map.contains_key(&old_id) {
                delete.push(old_t);
            }
        }

        TypeDiff {
            insert: insert,
            update: update,
            delete: delete,
        }
    }
}

pub struct StarblastDiff {
    pub new_ts: i64,

    pub system_diff: TypeDiff<System>,
    pub team_diff: TypeDiff<Team>,
    pub player_diff: TypeDiff<Player>,
    pub station_module_diff: TypeDiff<StationModule>,
    pub alien_scd_diff: TypeDiff<AlienSCD>,
    pub team_scd_diff: TypeDiff<TeamSCD>,
    pub player_scd_diff: TypeDiff<PlayerSCD>,
    pub station_module_scd_diff: TypeDiff<StationModuleSCD>,
}

impl StarblastDiff {
    pub fn new(old_state: Starblast, new_state: &Starblast) -> Self {
        StarblastDiff{
            new_ts: new_state.ts,
            system_diff: TypeDiff::new(old_state.systems, &new_state.systems),
            team_diff: TypeDiff::new(old_state.teams, &new_state.teams),
            player_diff: TypeDiff::new(old_state.players, &new_state.players),
            station_module_diff: TypeDiff::new(old_state.station_modules, &new_state.station_modules),
            alien_scd_diff: TypeDiff::new(old_state.alien_scds, &new_state.alien_scds),
            team_scd_diff: TypeDiff::new(old_state.team_scds, &new_state.team_scds),
            player_scd_diff: TypeDiff::new(old_state.player_scds, &new_state.player_scds),
            station_module_scd_diff: TypeDiff::new(old_state.station_module_scds, &new_state.station_module_scds),
        }
    }
}