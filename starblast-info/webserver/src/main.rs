#![allow(unused)]

mod differ;
mod error;
mod listener;
mod persister;
mod schema;

use error::Result;
use listener::listen;
use sqlx::postgres::PgPoolOptions;
use std::fs;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use tokio::join;
use tokio::{
    signal::unix::{signal, SignalKind},
    time,
};

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    let pg_user = fs::read_to_string("/run/secrets/starblast-info-pg-webserver-user").unwrap();
    let pg_password =
        fs::read_to_string("/run/secrets/starblast-info-pg-webserver-password").unwrap();
    let pg_db = fs::read_to_string("/run/secrets/starblast-info-pg-db").unwrap();

    let pg_uri = format!("postgres://{pg_user}:{pg_password}@starblast-info-pg/{pg_db}");

    let pool = Arc::new(
        PgPoolOptions::new()
            .max_connections(5)
            .connect(&pg_uri)
            .await
            .unwrap(),
    );

    let stop = Arc::new(AtomicBool::new(false));

    let listener = listen(stop.clone());

    tokio::spawn(persister::diff_and_persist(
        listener.state_rx,
        Arc::clone(&pool),
    ));

    let mut term_signal = signal(SignalKind::terminate()).expect("failed to setup signal handler");
    term_signal.recv().await;
    stop.store(true, Ordering::SeqCst);

    join!(listener.handle);

    Ok(())
}
