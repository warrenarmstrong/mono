mod dto;
mod system_listener;

use chrono::{Local, Timelike};
use dto::{ServerDTO, SimStatusDTO};
use reqwest::Error;
use std::collections::{HashMap, HashSet};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use system_listener::listen_system;
use tokio::{
    sync::{mpsc, watch, Mutex},
    task::JoinHandle,
    time::{self, Duration},
};

use self::system_listener::SystemListener;
use crate::schema::{
    AlienSCD, Player, PlayerSCD, Starblast, StarblastEntity, StationModule, StationModuleSCD,
    System, Team, TeamSCD,
};

pub struct Listener {
    pub handle: JoinHandle<()>,
    pub systems_rx: watch::Receiver<HashSet<i16>>,
    pub state_rx: mpsc::Receiver<Starblast>,
}

struct ListenerData {
    system_listeners: HashMap<i16, SystemListener>,
    systems_tx: watch::Sender<HashSet<i16>>,
    state_tx: mpsc::Sender<Starblast>,
}

pub fn listen(stop: Arc<AtomicBool>) -> Listener {
    let (systems_tx, systems_rx) = watch::channel(HashSet::new());
    let (state_tx, state_rx) = mpsc::channel(1000);

    let data = Arc::new(Mutex::new(ListenerData {
        system_listeners: HashMap::new(),
        systems_tx: systems_tx,
        state_tx: state_tx,
    }));

    let handle = tokio::spawn(listen_process(Arc::clone(&data), stop.clone()));
    tokio::spawn(publish_state(data, stop));

    Listener {
        handle: handle,
        systems_rx: systems_rx,
        state_rx: state_rx,
    }
}

fn remove_stale_system_listeners(data: &mut tokio::sync::MutexGuard<ListenerData>) {
    data.system_listeners.retain(|k, v| {
        if v.handle.is_finished() {
            log::info!("{} deleted system listener", k);
        };
        !v.handle.is_finished()
    });
}

async fn wait_until_next_second() {
    let millis = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .unwrap()
        .subsec_millis();
    time::sleep(Duration::from_millis((1000 - millis) as u64)).await;
}

async fn listen_process(data: Arc<Mutex<ListenerData>>, stop: Arc<AtomicBool>) {
    while !stop.load(Ordering::SeqCst) {
        wait_until_next_second().await;

        if (Local::now().second() % 10) != 0 {
            continue;
        }

        let response = match reqwest::Client::new()
            .get("https://starblast.io/simstatus.json")
            .send()
            .await
        {
            Ok(r) => r,
            Err(e) => {
                log::error!("Error sending request: {:?}", e);
                continue;
            }
        };

        let simstatus = match response.json::<SimStatusDTO>().await {
            Ok(simstatus) => simstatus,
            Err(e) => {
                log::error!("Error parsing response: {:?}", e);
                continue;
            }
        };

        log::info!("parsed simstatus");

        let mut data_gaurd = data.lock().await;

        remove_stale_system_listeners(&mut data_gaurd);

        for server in simstatus.servers {
            for system in server.systems {
                if system.mode == "team" && !data_gaurd.system_listeners.contains_key(&system.id) {
                    let system_listener =
                        listen_system(system.id, server.address.clone(), stop.clone());

                    data_gaurd
                        .system_listeners
                        .insert(system.id, system_listener);

                    log::info!("{} started system listener", system.id);
                }
            }
        }

        let keys = data_gaurd.system_listeners.keys().cloned().collect();

        match data_gaurd.systems_tx.send(keys) {
            Err(e) => log::error!("error transmitting systems: {:?}", e),
            Ok(()) => {}
        };

    }

    let mut data_gaurd = data.lock().await;

    let keys: Vec<i16> = data_gaurd.system_listeners.keys().map(|&key| key).collect();

    for key in keys {
        data_gaurd
            .system_listeners
            .remove(&key)
            .unwrap()
            .handle
            .await;
    }
}

async fn publish_state(data: Arc<Mutex<ListenerData>>, stop: Arc<AtomicBool>) {
    while !stop.load(Ordering::SeqCst) {
        wait_until_next_second().await;
        
        let mut data_gaurd = data.lock().await;
        remove_stale_system_listeners(&mut data_gaurd);

        let ts = Local::now().timestamp_millis() / 1000;

        let mut systems = HashMap::new();
        let mut teams = HashMap::new();
        let mut players = HashMap::new();
        let mut station_modules = HashMap::new();
        let mut alien_scds = HashMap::new();
        let mut team_scds = HashMap::new();
        let mut player_scds = HashMap::new();
        let mut station_module_scds = HashMap::new();

        for (system_sb_id, system_listener) in &data_gaurd.system_listeners {
            let system_sb_id = system_sb_id.to_owned();
            let state_gaurd = system_listener.state.lock().await;
            let system_listener_id = state_gaurd.system_listener_id;
            let ws_welcome_ts = state_gaurd.ws_welcome_ts;
            let ws_welcome_dto = &state_gaurd.ws_welcome_dto;
            let binary_200_dto = &state_gaurd.binary_200_dto;
            let binary_205_dto = &state_gaurd.binary_205_dto;
            let binary_206_dto = &state_gaurd.binary_206_dto;

            let ws_player_name_dtos = &state_gaurd.ws_player_name_dtos;

            if let Some(binary_206_dto) = binary_206_dto {
                for binary_206_alien_dto in &binary_206_dto.aliens {
                    let alien_scd = AlienSCD {
                        system_listener_id: system_listener_id,
                        system_sb_id: system_sb_id,
                        alien_sb_id: binary_206_alien_dto.id,
                        x: binary_206_alien_dto.x,
                        y: binary_206_alien_dto.y,
                    };
                    alien_scds.insert(alien_scd.entity_id(), alien_scd);
                }
            }

            if let Some(binary_200_dto) = binary_200_dto {
                for binary_200_player_dto in &binary_200_dto.players {
                    let player_scd = PlayerSCD {
                        system_listener_id: system_listener_id,
                        system_sb_id: system_sb_id,
                        player_sb_id: binary_200_player_dto.id,
                        score: binary_200_player_dto.score.try_into().unwrap(),
                        ship_id: binary_200_player_dto.ship_id.try_into().unwrap(),
                        is_alive: binary_200_player_dto.is_alive,
                        x: binary_200_player_dto.x,
                        y: binary_200_player_dto.y,
                    };
                    player_scds.insert(player_scd.entity_id(), player_scd);
                }
            }

            if let Some(binary_205_dto) = binary_205_dto {
                for team_sb_id in 0..=2 as i16 {
                    let binary_205_team_dto =
                        binary_205_dto.teams.get(team_sb_id as usize).unwrap();
                    let team_scd = TeamSCD {
                        system_listener_id: system_listener_id,
                        system_sb_id: system_sb_id,
                        team_sb_id: team_sb_id,
                        is_open: binary_205_team_dto.is_open,
                        base_level: binary_205_team_dto.base_level.into(),
                        crystals: binary_205_team_dto.crystals.into(),
                    };
                    team_scds.insert(team_scd.entity_id(), team_scd);

                    for binary_205_module_dto in &binary_205_team_dto.modules {
                        let station_module_scd = StationModuleSCD {
                            system_listener_id: system_listener_id,
                            system_sb_id: system_sb_id,
                            team_sb_id: team_sb_id,
                            station_module_sb_id: binary_205_module_dto.id,
                            status: binary_205_module_dto.status,
                        };
                        station_module_scds
                            .insert(station_module_scd.entity_id(), station_module_scd);
                    }
                }
            }

            if let (Some(ws_welcome_dto), Some(ws_welcome_ts)) = (ws_welcome_dto, ws_welcome_ts) {
                for (player_sb_id, ws_player_name_dto) in ws_player_name_dtos {
                    let player = match &ws_player_name_dto.data.custom {
                        Some(custom) => Player {
                            system_listener_id: system_listener_id,
                            system_sb_id: system_sb_id,
                            team_sb_id: ws_player_name_dto.data.friendly.into(),
                            player_sb_id: ws_player_name_dto.data.id.into(),
                            name: ws_player_name_dto.data.player_name.clone(),
                            hue: ws_player_name_dto.data.hue,
                            custom_badge: Some(custom.badge.clone()),
                            custom_finish: custom.finish.clone(),
                            custom_laser: custom.laser.clone(),
                            custom_hue: Some(custom.hue),
                        },
                        None => Player {
                            system_listener_id: system_listener_id,
                            system_sb_id: system_sb_id,
                            team_sb_id: ws_player_name_dto.data.friendly.into(),
                            player_sb_id: ws_player_name_dto.data.id.into(),
                            name: ws_player_name_dto.data.player_name.clone(),
                            hue: ws_player_name_dto.data.hue,
                            custom_badge: None,
                            custom_finish: None,
                            custom_laser: None,
                            custom_hue: None,
                        },
                    };
                    players.insert(player.entity_id(), player);
                }

                for team_sb_id in 0..=2 as i16 {
                    let ws_welcome_team_dto = ws_welcome_dto
                        .data
                        .mode
                        .teams
                        .get(team_sb_id as usize)
                        .unwrap();
                    let team = Team {
                        system_listener_id: system_listener_id,
                        system_sb_id: system_sb_id,
                        team_sb_id: team_sb_id,
                        faction: ws_welcome_team_dto.faction.clone(),
                        base_name: ws_welcome_team_dto.base_name.clone(),
                        hue: ws_welcome_team_dto.hue,
                        station_phase: ws_welcome_team_dto.station.phase,
                    };
                    teams.insert(team.entity_id(), team);

                    for ws_welcome_module_dto in &ws_welcome_team_dto.station.modules {
                        let station_module = StationModule {
                            system_listener_id: system_listener_id,
                            system_sb_id: system_sb_id,
                            team_sb_id: team_sb_id,
                            station_module_sb_id: ws_welcome_module_dto.id.into(),
                            r#type: ws_welcome_module_dto.r#type.clone(),
                            x: ws_welcome_module_dto.x,
                            y: ws_welcome_module_dto.y,
                            dir: ws_welcome_module_dto.dir,
                        };
                        station_modules.insert(station_module.entity_id(), station_module);
                    }
                }
                let system = System {
                    system_listener_id: system_listener_id,
                    system_sb_id: system_sb_id,
                    server_address: state_gaurd.server_address.clone(),
                    version: ws_welcome_dto.data.version,
                    welcome_ts: ws_welcome_ts,
                    listener_version: 1,
                    seed: ws_welcome_dto.data.seed,
                    servertime: ws_welcome_dto.data.servertime,
                    name: ws_welcome_dto.data.name.clone(),
                    size: ws_welcome_dto.data.size,
                    region: ws_welcome_dto.data.region.clone(),
                    mode: ws_welcome_dto.data.mode.id.clone(),
                    max_players: ws_welcome_dto.data.mode.max_players,
                    crystal_value: ws_welcome_dto.data.mode.crystal_value,
                    crystal_drop: ws_welcome_dto.data.mode.crystal_drop,
                    map_size: ws_welcome_dto.data.mode.map_size,
                    lives: ws_welcome_dto.data.mode.lives,
                    max_level: ws_welcome_dto.data.mode.max_level,
                    friendly_colors: ws_welcome_dto.data.mode.friendly_colors,
                    close_time: ws_welcome_dto.data.mode.close_time,
                    close_number: ws_welcome_dto.data.mode.close_number,
                    unlisted: ws_welcome_dto.data.mode.unlisted,
                    survival_time: ws_welcome_dto.data.mode.survival_time,
                    survival_level: ws_welcome_dto.data.mode.survival_level,
                    starting_ship: ws_welcome_dto.data.mode.starting_ship,
                    starting_ship_maxed: ws_welcome_dto.data.mode.starting_ship_maxed,
                    asteroids_strength: ws_welcome_dto.data.mode.asteroids_strength,
                    friction_ratio: ws_welcome_dto.data.mode.friction_ratio,
                    speed_mod: ws_welcome_dto.data.mode.speed_mod,
                    rcs_toggle: ws_welcome_dto.data.mode.rcs_toggle,
                    weapon_drop: ws_welcome_dto.data.mode.weapon_drop,
                    mines_self_destroy: ws_welcome_dto.data.mode.mines_self_destroy,
                    mines_destroy_delay: ws_welcome_dto.data.mode.mines_destroy_delay,
                    healing_enabled: ws_welcome_dto.data.mode.healing_enabled,
                    healing_ratio: ws_welcome_dto.data.mode.healing_ratio,
                    shield_regen_factor: ws_welcome_dto.data.mode.shield_regen_factor,
                    power_regen_factor: ws_welcome_dto.data.mode.power_regen_factor,
                    auto_refill: ws_welcome_dto.data.mode.auto_refill,
                    projectile_speed: ws_welcome_dto.data.mode.projectile_speed,
                    strafe: ws_welcome_dto.data.mode.strafe,
                    release_crystal: ws_welcome_dto.data.mode.release_crystal,
                    large_grid: ws_welcome_dto.data.mode.large_grid,
                    bouncing_lasers: ws_welcome_dto.data.mode.bouncing_lasers,
                    max_tier_lives: ws_welcome_dto.data.mode.max_tier_lives,
                    auto_assign_teams: ws_welcome_dto.data.mode.auto_assign_teams,
                    station_size: ws_welcome_dto.data.mode.station_size,
                    crystal_capacity: ws_welcome_dto.data.mode.crystal_capacity.clone(),
                    deposit_regen: ws_welcome_dto.data.mode.deposit_regen.clone(),
                    deposit_shield: ws_welcome_dto.data.mode.deposit_shield.clone(),
                    spawning_shield: ws_welcome_dto.data.mode.spawning_shield.clone(),
                    structure_shield: ws_welcome_dto.data.mode.structure_shield.clone(),
                    spawning_regen: ws_welcome_dto.data.mode.spawning_regen.clone(),
                    structure_regen: ws_welcome_dto.data.mode.structure_regen.clone(),
                    repair_threshold: ws_welcome_dto.data.mode.repair_threshold,
                    all_ships_can_dock: ws_welcome_dto.data.mode.all_ships_can_dock,
                    all_ships_can_respawn: ws_welcome_dto.data.mode.all_ships_can_respawn,
                };
                systems.insert(system.entity_id(), system);
            }
        }

        let starblast = Starblast {
            ts: ts,
            systems: systems,
            teams: teams,
            players: players,
            station_modules: station_modules,
            alien_scds: alien_scds,
            team_scds: team_scds,
            player_scds: player_scds,
            station_module_scds: station_module_scds,
        };

        match data_gaurd.state_tx.send(starblast).await {
            Err(e) => log::error!("error transmitting state: {:?}", e),
            Ok(()) => {}
        }
    }
}
