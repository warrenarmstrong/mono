use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use uuid::Uuid;

pub trait StarblastEntity: PartialEq + Clone {
    fn entity_id(&self) -> u64;
}

#[derive(Debug, PartialEq, Clone)]
pub struct AlienSCD {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub alien_sb_id: u16,

    pub x: u8,
    pub y: u8,
}

impl StarblastEntity for AlienSCD {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (self.alien_sb_id, self.system_sb_id).hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct PlayerSCD {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub player_sb_id: u8,

    pub score: i32,
    pub ship_id: i16,
    pub is_alive: bool,
    pub x: u8,
    pub y: u8,
}

impl StarblastEntity for PlayerSCD {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (self.player_sb_id, self.system_sb_id).hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Player {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub team_sb_id: i16,
    pub player_sb_id: i16,

    pub name: String,
    pub hue: i16,
    pub custom_badge: Option<String>,
    pub custom_finish: Option<String>,
    pub custom_laser: Option<String>,
    pub custom_hue: Option<i16>,
}

impl StarblastEntity for Player {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (self.player_sb_id, self.team_sb_id, self.system_sb_id).hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct StationModuleSCD {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub team_sb_id: i16,
    pub station_module_sb_id: u8,

    pub status: u8,
}

impl StarblastEntity for StationModuleSCD {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (
            self.station_module_sb_id,
            self.team_sb_id,
            self.system_sb_id,
        )
            .hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct StationModule {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub team_sb_id: i16,
    pub station_module_sb_id: i16,

    pub r#type: String,
    pub x: f64,
    pub y: f64,
    pub dir: u8,
}

impl StarblastEntity for StationModule {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (
            self.station_module_sb_id,
            self.team_sb_id,
            self.system_sb_id,
        )
            .hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct TeamSCD {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub team_sb_id: i16,

    pub is_open: bool,
    pub base_level: i16,
    pub crystals: i64,
}

impl StarblastEntity for TeamSCD {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (self.team_sb_id, self.system_sb_id).hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Team {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub team_sb_id: i16,

    pub faction: String,
    pub base_name: String,
    pub hue: i16,
    pub station_phase: f64,
}

impl StarblastEntity for Team {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (self.team_sb_id, self.system_sb_id).hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct System {
    pub system_listener_id: Uuid,
    pub system_sb_id: i16,
    pub server_address: String,

    pub welcome_ts: i64,

    pub listener_version: i16,
    pub version: i16,
    pub seed: i16,
    pub servertime: i32,
    pub name: String,
    pub size: i32,
    pub region: String,
    pub mode: String,
    pub max_players: i32,
    pub crystal_value: f64,
    pub crystal_drop: i32,
    pub map_size: i32,
    pub lives: i32,
    pub max_level: i32,
    pub friendly_colors: i32,
    pub close_time: i32,
    pub close_number: i32,
    pub unlisted: bool,
    pub survival_time: i32,
    pub survival_level: i32,
    pub starting_ship: i32,
    pub starting_ship_maxed: bool,
    pub asteroids_strength: f64,
    pub friction_ratio: f64,
    pub speed_mod: f64,
    pub rcs_toggle: bool,
    pub weapon_drop: i32,
    pub mines_self_destroy: bool,
    pub mines_destroy_delay: i32,
    pub healing_enabled: bool,
    pub healing_ratio: f64,
    pub shield_regen_factor: f64,
    pub power_regen_factor: f64,
    pub auto_refill: bool,
    pub projectile_speed: f64,
    pub strafe: f64,
    pub release_crystal: bool,
    pub large_grid: bool,
    pub bouncing_lasers: i32,
    pub max_tier_lives: i32,
    pub auto_assign_teams: bool,
    pub station_size: i32,
    pub crystal_capacity: Vec<i32>,
    pub deposit_shield: Vec<i32>,
    pub spawning_shield: Vec<i32>,
    pub structure_shield: Vec<i32>,
    pub deposit_regen: Vec<i32>,
    pub spawning_regen: Vec<i32>,
    pub structure_regen: Vec<i32>,
    pub repair_threshold: f64,
    pub all_ships_can_dock: bool,
    pub all_ships_can_respawn: bool,
}

impl StarblastEntity for System {
    fn entity_id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        (self.system_sb_id).hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Default)]
pub struct Starblast {
    pub ts: i64,

    pub systems: HashMap<u64, System>,
    pub teams: HashMap<u64, Team>,
    pub players: HashMap<u64, Player>,
    pub station_modules: HashMap<u64, StationModule>,

    pub alien_scds: HashMap<u64, AlienSCD>,
    pub team_scds: HashMap<u64, TeamSCD>,
    pub player_scds: HashMap<u64, PlayerSCD>,
    pub station_module_scds: HashMap<u64, StationModuleSCD>,
}
