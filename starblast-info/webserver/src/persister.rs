use std::collections::HashMap;
use std::sync::Arc;

use uuid::Uuid;
use serde::de;
use tokio::sync::mpsc::Receiver;
use tokio::join;
use crate::schema::{
    AlienSCD,
    PlayerSCD,
    StationModuleSCD,
    TeamSCD,
    Player,
    StationModule,
    Team,
    System,
    Starblast,
};
use crate::differ::StarblastDiff;
use sqlx::{Execute, Pool, Postgres, QueryBuilder};

pub async fn diff_and_persist(mut state_rx: Receiver<Starblast>, pool: Arc<Pool<Postgres>>) {
    let mut old_state = Starblast{
        ts: 0,
        systems: HashMap::new(),
        teams: HashMap::new(),
        players: HashMap::new(),
        station_modules: HashMap::new(),
        alien_scds: HashMap::new(),
        team_scds: HashMap::new(),
        player_scds: HashMap::new(),
        station_module_scds: HashMap::new(),
    };
    
    while let Some(new_state) = state_rx.recv().await {
        let diff = StarblastDiff::new(old_state, &new_state);

        persist(&diff, &new_state, pool.as_ref()).await;

        old_state = new_state;
    }
}

async fn persist(diff: &StarblastDiff, new_state: &Starblast, pool: &Pool<Postgres>) {
    persist_systems(diff, pool).await;
    persist_teams(diff, pool).await;
    
    join!(
        persist_players(diff, pool),
        persist_station_modules(diff, pool),

        persist_team_scds(diff, pool),
        persist_player_scds(diff, pool),
        persist_station_module_scds(diff, pool),
        persist_alien_scds(diff, pool),

        update_latest_ts(new_state, pool),
    );
}

// async fn persist(diff: &StarblastDiff, new_state: &Starblast, pool: &Pool<Postgres>) {
//     persist_systems(diff, pool).await;
//     persist_teams(diff, pool).await;
//     persist_players(diff, pool).await;
//     persist_station_modules(diff, pool).await;

//     persist_team_scds(diff, pool).await;
//     persist_player_scds(diff, pool).await;
//     persist_station_module_scds(diff, pool).await;
//     persist_alien_scds(diff, pool).await;

//     update_latest_ts(new_state, pool).await;
// }

async fn persist_systems(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    if diff.system_diff.insert.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.system (
            system_listener_id,
            system_sb_id,
            server_address,
            welcome_ts,
            listener_version,
            version,
            seed,
            servertime,
            name,
            size,
            region,
            mode,
            max_players,
            crystal_value,
            crystal_drop,
            map_size,
            lives,
            max_level,
            friendly_colors,
            close_time,
            close_number,
            unlisted,
            survival_time,
            survival_level,
            starting_ship,
            starting_ship_maxed,
            asteroids_strength,
            friction_ratio,
            speed_mod,
            rcs_toggle,
            weapon_drop,
            mines_self_destroy,
            mines_destroy_delay,
            healing_enabled,
            healing_ratio,
            shield_regen_factor,
            power_regen_factor,
            auto_refill,
            projectile_speed,
            strafe,
            release_crystal,
            large_grid,
            bouncing_lasers,
            max_tier_lives,
            auto_assign_teams,
            station_size,
            crystal_capacity,
            deposit_shield,
            spawning_shield,
            structure_shield,
            deposit_regen,
            spawning_regen,
            structure_regen,
            repair_threshold,
            all_ships_can_dock,
            all_ships_can_respawn,
            latest_ts
        ) "
    );

    query_builder.push_values(&diff.system_diff.insert, |mut b, system| {
        b.push_bind(system.system_listener_id)
            .push_bind(system.system_sb_id)
            .push_bind(system.server_address.clone())
            .push_bind(system.welcome_ts)
            .push_bind(system.listener_version)
            .push_bind(system.version)
            .push_bind(system.seed)
            .push_bind(system.servertime)
            .push_bind(system.name.clone())
            .push_bind(system.size)
            .push_bind(system.region.clone())
            .push_bind(system.mode.clone())
            .push_bind(system.max_players)
            .push_bind(system.crystal_value)
            .push_bind(system.crystal_drop)
            .push_bind(system.map_size)
            .push_bind(system.lives)
            .push_bind(system.max_level)
            .push_bind(system.friendly_colors)
            .push_bind(system.close_time)
            .push_bind(system.close_number)
            .push_bind(system.unlisted)
            .push_bind(system.survival_time)
            .push_bind(system.survival_level)
            .push_bind(system.starting_ship)
            .push_bind(system.starting_ship_maxed)
            .push_bind(system.asteroids_strength)
            .push_bind(system.friction_ratio)
            .push_bind(system.speed_mod)
            .push_bind(system.rcs_toggle)
            .push_bind(system.weapon_drop)
            .push_bind(system.mines_self_destroy)
            .push_bind(system.mines_destroy_delay)
            .push_bind(system.healing_enabled)
            .push_bind(system.healing_ratio)
            .push_bind(system.shield_regen_factor)
            .push_bind(system.power_regen_factor)
            .push_bind(system.auto_refill)
            .push_bind(system.projectile_speed)
            .push_bind(system.strafe)
            .push_bind(system.release_crystal)
            .push_bind(system.large_grid)
            .push_bind(system.bouncing_lasers)
            .push_bind(system.max_tier_lives)
            .push_bind(system.auto_assign_teams)
            .push_bind(system.station_size)
            .push_bind(system.crystal_capacity.clone())
            .push_bind(system.deposit_shield.clone())
            .push_bind(system.spawning_shield.clone())
            .push_bind(system.structure_shield.clone())
            .push_bind(system.deposit_regen.clone())
            .push_bind(system.spawning_regen.clone())
            .push_bind(system.structure_regen.clone())
            .push_bind(system.repair_threshold)
            .push_bind(system.all_ships_can_dock)
            .push_bind(system.all_ships_can_respawn)
            .push_bind(diff.new_ts);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", diff.system_diff.insert);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_teams(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    if diff.team_diff.insert.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.team (
            system_listener_id, team_sb_id, faction, base_name, hue, station_phase
        ) "
    );

    query_builder.push_values(&diff.team_diff.insert, |mut b, team| {
        b.push_bind(team.system_listener_id)
            .push_bind(team.team_sb_id)
            .push_bind(team.faction.clone())
            .push_bind(team.base_name.clone())
            .push_bind(team.hue)
            .push_bind(team.station_phase);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", diff.team_diff.insert);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_players(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    if diff.player_diff.insert.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.player (
            system_listener_id, team_sb_id, player_sb_id, name, hue, custom_badge, custom_finish, custom_laser, custom_hue
        ) "
    );

    query_builder.push_values(&diff.player_diff.insert, |mut b, player| {
        b.push_bind(player.system_listener_id)
            .push_bind(player.team_sb_id)
            .push_bind(player.player_sb_id)
            .push_bind(player.name.clone())
            .push_bind(player.hue)
            .push_bind(player.custom_badge.clone())
            .push_bind(player.custom_finish.clone())
            .push_bind(player.custom_laser.clone())
            .push_bind(player.custom_hue);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", diff.player_diff.insert);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_station_modules(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    if diff.station_module_diff.insert.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.station_module (
            system_listener_id, team_sb_id, station_module_sb_id, type, x, y, dir
        ) "
    );

    query_builder.push_values(&diff.station_module_diff.insert, |mut b, station_module| {
        b.push_bind(station_module.system_listener_id)
            .push_bind(station_module.team_sb_id)
            .push_bind(station_module.station_module_sb_id)
            .push_bind(station_module.r#type.clone())
            .push_bind(station_module.x)
            .push_bind(station_module.y)
            .push_bind::<i16>(station_module.dir.into());
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", diff.station_module_diff.insert);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_team_scds(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    let mut team_scds = Vec::new();

    for team_scd in diff.team_scd_diff.insert.iter().chain(diff.team_scd_diff.update.iter()) {
        team_scds.push((team_scd, false))
    }

    for team_scd in diff.team_scd_diff.delete.iter() {
        team_scds.push((team_scd, true));
    }

    if team_scds.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.team_scd (
            system_listener_id, team_sb_id, ts, is_open, base_level, crystals, is_deleted
        ) "
    );

    query_builder.push_values(&team_scds, |mut b, (team_scd, is_deleted)| {
        b.push_bind(team_scd.system_listener_id)
            .push_bind(team_scd.team_sb_id)
            .push_bind(diff.new_ts)
            .push_bind(team_scd.is_open)
            .push_bind(team_scd.base_level)
            .push_bind(team_scd.crystals)
            .push_bind(is_deleted);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", team_scds);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_player_scds(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    let mut player_scds = Vec::new();

    for player_scd in diff.player_scd_diff.insert.iter().chain(diff.player_scd_diff.update.iter()) {
        player_scds.push((player_scd, false))
    }

    for player_scd in diff.player_scd_diff.delete.iter() {
        player_scds.push((player_scd, true));
    }

    if player_scds.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.player_scd (
            system_listener_id, player_sb_id, ts, score, ship_id, is_alive, x, y, is_deleted
        ) "
    );

    query_builder.push_values(&player_scds, |mut b, (player_scd, is_deleted)| {
        b.push_bind(player_scd.system_listener_id)
            .push_bind::<i16>(player_scd.player_sb_id.into())
            .push_bind(diff.new_ts)
            .push_bind(player_scd.score)
            .push_bind(player_scd.ship_id)
            .push_bind(player_scd.is_alive)
            .push_bind::<i16>(player_scd.x.into())
            .push_bind::<i16>(player_scd.y.into())
            .push_bind(is_deleted);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", player_scds);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_station_module_scds(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    let mut station_module_scds = Vec::new();

    for station_module_scd in diff.station_module_scd_diff.insert.iter().chain(diff.station_module_scd_diff.update.iter()) {
        station_module_scds.push((station_module_scd, false))
    }

    for station_module_scd in diff.station_module_scd_diff.delete.iter() {
        station_module_scds.push((station_module_scd, true));
    }

    if station_module_scds.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.station_module_scd (
            system_listener_id, team_sb_id, station_module_sb_id, ts, status, is_deleted
        ) "
    );

    query_builder.push_values(&station_module_scds, |mut b, (station_module_scds, is_deleted)| {
        b.push_bind(station_module_scds.system_listener_id)
            .push_bind(station_module_scds.team_sb_id)
            .push_bind::<i16>(station_module_scds.station_module_sb_id.into())
            .push_bind(diff.new_ts)
            .push_bind::<i16>(station_module_scds.status.into())
            .push_bind(is_deleted);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", station_module_scds);
            log::error!("{:?}", e);
        }
    };
}

async fn persist_alien_scds(diff: &StarblastDiff, pool: &Pool<Postgres>) {
    let mut alien_scds = Vec::new();

    for alien_scd in diff.alien_scd_diff.insert.iter().chain(diff.alien_scd_diff.update.iter()) {
        alien_scds.push((alien_scd, false))
    }

    for alien_scd in diff.alien_scd_diff.delete.iter() {
        alien_scds.push((alien_scd, true));
    }

    if alien_scds.len() == 0 {
        return;
    }

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new(
        "INSERT INTO listener.alien_scd (
            system_listener_id, alien_sb_id, ts, x, y, is_deleted
        ) "
    );

    query_builder.push_values(&alien_scds, |mut b, (alien_scd, is_deleted)| {
        b.push_bind(alien_scd.system_listener_id)
            .push_bind::<i32>(alien_scd.alien_sb_id.into())
            .push_bind(diff.new_ts)
            .push_bind::<i16>(alien_scd.x.into())
            .push_bind::<i16>(alien_scd.y.into())
            .push_bind(is_deleted);
    });

    match query_builder.build().execute(pool).await {
        Ok(r) => {},
        Err(e) => {
            log::error!("{:?}", alien_scds);
            log::error!("{:?}", e);
        }
    };
}

async fn update_latest_ts(new_state: &Starblast, pool: &Pool<Postgres>) {
    let system_listener_ids: Vec<Uuid> = new_state.systems.values().map(|s| s.system_listener_id).collect();

    sqlx::query("UPDATE listener.system SET latest_ts = $1 WHERE system_listener_id = ANY($2)")
        .bind(new_state.ts)
        .bind(&system_listener_ids[..])
        .execute(pool)
        .await.unwrap_or_default();
}