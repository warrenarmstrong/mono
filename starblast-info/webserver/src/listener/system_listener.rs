use chrono::{Local, Timelike};
use futures_util::{stream::StreamExt, SinkExt};
use rand::Rng;
use serde_json::{json, Error as SJError};
use std::collections::{HashMap, HashSet};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use tokio::{
    sync::{watch, Mutex},
    task::JoinHandle,
};
use tokio_tungstenite::{
    connect_async, tungstenite::client::IntoClientRequest, tungstenite::protocol::Message,
    tungstenite::Error,
};
use uuid::Uuid;

use super::dto::{Binary200DTO, Binary205DTO, Binary206DTO, WSPlayerNameDTO, WSWelcomeDTO, WSDTO};

pub struct SystemListener {
    pub handle: JoinHandle<()>,
    pub ws_rx: watch::Receiver<Option<Message>>,
    pub state: Arc<Mutex<SystemState>>,
}

pub struct SystemState {
    pub system_listener_id: Uuid,
    pub system_id: i16,
    pub server_address: String,

    pub ws_welcome_dto: Option<WSWelcomeDTO>,
    pub ws_player_name_dtos: HashMap<u8, WSPlayerNameDTO>,

    pub binary_200_dto: Option<Binary200DTO>,
    pub binary_205_dto: Option<Binary205DTO>,
    pub binary_206_dto: Option<Binary206DTO>,

    pub ws_welcome_ts: Option<i64>,
}

pub fn listen_system(
    system_id: i16,
    server_address: String,
    stop: Arc<AtomicBool>,
) -> SystemListener {
    let (ws_tx, ws_rx) = watch::channel(None);

    let state = Arc::new(Mutex::new(SystemState {
        system_listener_id: Uuid::new_v4(),
        system_id: system_id,
        server_address: server_address,

        ws_welcome_dto: None,
        ws_player_name_dtos: HashMap::new(),

        binary_200_dto: None,
        binary_205_dto: None,
        binary_206_dto: None,

        ws_welcome_ts: None,
    }));

    let handle = tokio::spawn(listen_system_error_handle(Arc::clone(&state), ws_tx, stop));

    SystemListener {
        handle: handle,
        ws_rx: ws_rx,
        state: state,
    }
}

async fn listen_system_error_handle(
    state: Arc<Mutex<SystemState>>,
    ws_tx: watch::Sender<Option<Message>>,
    stop: Arc<AtomicBool>,
) {
    let system_id = state.lock().await.system_id.clone();

    if let Err(e) = listen_system_process(state, ws_tx, stop).await {
        log::error!("{} error while listening: {}", system_id, e);
    }
}

async fn listen_system_process(
    state: Arc<Mutex<SystemState>>,
    ws_tx: watch::Sender<Option<Message>>,
    stop: Arc<AtomicBool>,
) -> Result<(), Error> {
    let (system_id, server_address) = {
        let state_guard = state.lock().await;
        (state_guard.system_id, state_guard.server_address.clone())
    };

    let (ip, port) = server_address.split_once(':').unwrap();
    let dashed_ip = ip.replace(".", "-");
    let websocket_endpoint = format!("wss://{dashed_ip}.starblast.io:{port}/");

    let mut request = websocket_endpoint.into_client_request()?;
    let headers = request.headers_mut();
    headers.insert("Origin", "https://starblast.io".parse()?);

    let (ws_stream, _) = connect_async(request).await?;

    let (mut write, mut read) = ws_stream.split();

    let initial_message = Message::Text(get_initial_message(system_id).unwrap());
    write.send(initial_message).await?;

    while let Some(message_result) = read.next().await {
        if stop.load(Ordering::SeqCst) {
            write.send(Message::Close(None)).await?;
            break;
        }

        let message = match message_result {
            Ok(message) => message,
            Err(e) => {
                log::error!("{} error reading message: {}", system_id, e);
                continue;
            }
        };

        match ws_tx.send(Some(message.clone())) {
            Err(e) => log::error!(
                "{:?} error transmitting websocket message {:?}",
                system_id,
                e
            ),
            Ok(()) => {}
        };

        let ws_dto = match WSDTO::parse(message.clone()) {
            Ok(ws_dto) => ws_dto,
            Err(e) => {
                log::error!(
                    "{} error parsing WSDTO error: {} message: {}",
                    system_id,
                    e,
                    message
                );
                continue;
            }
        };

        if let WSDTO::Binary200DTO(_) = ws_dto {
            let text = rand::Rng::gen_range(&mut rand::rngs::OsRng, 0..=359).to_string();
            write.send(Message::Text(text)).await?;
        }

        let mut state_gaurd = state.lock().await;
        match ws_dto {
            WSDTO::WSWelcomeDTO(ws_welcome_dto) => {
                state_gaurd.ws_welcome_ts = Some(Local::now().timestamp_millis() / 1000);
                state_gaurd.ws_welcome_dto = Some(ws_welcome_dto);
                log::info!("{} parsed WSWelcome", system_id)
            }
            WSDTO::WSPlayerNameDTO(ws_player_name_dto) => {
                state_gaurd
                    .ws_player_name_dtos
                    .insert(ws_player_name_dto.data.id, ws_player_name_dto);
            }
            WSDTO::CannotJoin => {
                log::info!("{} recieved cannot_join", system_id);
                write.send(Message::Close(None)).await?;
                break;
            }
            WSDTO::Binary200DTO(binary_200_dto) => {
                let binary_player_ids: HashSet<u8> =
                    HashSet::from_iter(binary_200_dto.players.iter().map(|p| p.id));

                state_gaurd
                    .ws_player_name_dtos
                    .retain(|id, _| binary_player_ids.contains(id));

                let current_player_ids: HashSet<u8> =
                    state_gaurd.ws_player_name_dtos.keys().copied().collect();

                let messages: Vec<Message> = binary_200_dto
                    .players
                    .iter()
                    .filter(|p| !current_player_ids.contains(&p.id))
                    .map(|p| json!({"name": "get_name", "data": {"id": p.id}}))
                    .map(|j| serde_json::to_string(&j).unwrap())
                    .map(|s| Message::Text(s))
                    .collect();

                for message in messages {
                    write.send(message).await?
                }

                state_gaurd.binary_200_dto = Some(binary_200_dto);
            }
            WSDTO::Binary205DTO(binary_205_dto) => {
                state_gaurd.binary_205_dto = Some(binary_205_dto)
            }
            WSDTO::Binary206DTO(binary_206_dto) => {
                state_gaurd.binary_206_dto = Some(binary_206_dto)
            }
            WSDTO::Ping(ping) => {
                log::warn!("{} recieved ping: {:?}", system_id, ping)
            }
            WSDTO::Pong(pong) => {
                log::warn!("{} recieved pong: {:?}", system_id, pong)
            }
            WSDTO::Close(close_frame_option) => {
                log::info!(
                    "{} recieved close frame: {:?}",
                    system_id,
                    close_frame_option
                )
            }
        }
    }

    Ok(())
}

fn get_initial_message(system_id: i16) -> Result<String, SJError> {
    let mut rng = rand::thread_rng();

    let client_ship_id: String = rng
        .gen_range(100000000000000000 as i64..999999999999999999 as i64)
        .to_string();

    let player_name = NAMES[rng.gen_range(0..NAMES.len())];

    let initial_message = json!({
        "name": "ojct:4",
        "data": {
            "mode": "join",
            "player_name": player_name,
            "hue": 12,
            "preferred": system_id,
            "bonus": false,
            "ecp_key": null,
            "steamid": null,
            "ecp_custom": {
                "badge": "star",
                "finish": "alloy",
                "laser": "1",
            },
            "create": false,
            "client_ship_id": client_ship_id,
            "client_tr": 1,
        },
    });

    serde_json::to_string(&initial_message)
}

pub static NAMES: &[&'static str] = &[
    "ARKADY DARELL",
    "BEL RIOSE",
    "CLEON I",
    "DORS VENABILI",
    "EBLING MIS",
    "GAAL DORNICK",
    "HARI SELDON",
    "HOBER MALLOW",
    "JANOV PELORAT",
    "THE MULE",
    "PREEM PALVER",
    "R.D. OLIVAW",
    "R.G. REVENTLOV",
    "RAYCH SELDON",
    "SALVOR HARDIN",
    "WANDA SELDON",
    "YUGO AMARYL",
    "JAMES T. KIRK",
    "LEONARD MCCOY",
    "HIKARU SULU",
    "MONTGOMERY SCOTT",
    "SPOCK",
    "PICARD",
    "CHRISTINE CHAPEL",
    "NYOTA UHURA",
    "PAVEL CHEKOV",
    "FORD",
    "ZAPHOD",
    "MARVIN",
    "ANAKIN",
    "LUKE",
    "LEIA",
    "ACKBAR",
    "TARKIN",
    "JABBA",
    "REY",
    "KYLO",
    "HAN",
    "VADER",
    "D.A.R.Y.L.",
    "HAL 9000",
    "LYTA ALEXANDER",
    "STEPHEN FRANKLIN",
    "LENNIER",
];
