containers=$(docker ps -a --filter "status=exited" --format "{{.ID}}")

if [ ! -z "$containers" ]; then
    docker rm $containers
fi