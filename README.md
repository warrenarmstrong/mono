# Starblast Info

The Starblast Info project streams real time telemetry data from all active starblast.io matches. It then transforms, encodes, compresses, and stores the data for later analysis. It uses a number of technologies to achieve this including a Rust application, a PostgreSQL database, an open source dagster deployment, and cloudflare R2.

The live running dagster deployment can be viewed at https://dagster.starblast.info

## Project Structure

The elements of the project can be viewed at these directories in the repo:

```
dagster 
└── mono 
    └── mono
        ├── shared (shared dagster constructs such as resources and io managers)
        └── starblast-info (starblast info specific dagster constructs such as assets and jobs)
starblast-info
├── migration (sql migrations for the starblast info PostgreSQL database)
└── webserver (rust application that maintains data streams)
```

## Data Flow

```mermaid
graph LR

A[Starblast.io match 1] -- websocket stream --> R[Rust Application]
B[Starblast.io match 2] -- websocket stream --> R
C[Starblast.io match 3] -- websocket stream --> R
E[Starblast.io match ...] -- websocket stream --> R
R --> P[PostgreSQL Database]
P --> D[Dagster]
D --> S[Cloudflare R2]
```

## Data Warehouse

The final destination for the telemetry data is a set of tables composed of hive partitioned parquet files in a Cloudflare R2 bucket. These tables follow the structure expressed by the following ER diagram:

![ER Diagram](./images/er_diagram.PNG)