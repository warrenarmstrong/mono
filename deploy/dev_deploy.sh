OLD_CONTAINER=$(docker ps -aqf "name=starblast-info-webserver")

docker compose -f deploy/compose.yml -f deploy/compose.dev.yml build
docker compose -f deploy/compose.yml -f deploy/compose.dev.yml up -d --no-deps --remove-orphans --build --scale starblast-info-webserver=2 --no-recreate starblast-info-webserver

sleep 60

docker container rm -f $OLD_CONTAINER
docker compose up -f deploy/compose.yml -f deploy/compose.dev.yml up --no-deps --remove-orphans --build --scale starblast-info-webserver=1 --no-recreate starblast-info-webserver