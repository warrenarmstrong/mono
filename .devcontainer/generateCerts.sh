apt-get update
apt-get install openssl

mkdir volumes/cert/live



(
    mkdir volumes/cert/live/pgadmin.devwarrenarmstrong.io &&
    cd volumes/cert/live/pgadmin.devwarrenarmstrong.io &&
    openssl genrsa -out privkey.pem 2048 &&
    openssl req -new -x509 -key privkey.pem -out fullchain.pem -days 365 -subj "/C=US/ST=State/L=City/O=Organization/OU=OrganizationUnit/CN=www.example.com"
)

(
    mkdir volumes/cert/live/dagster.devwarrenarmstrong.io &&
    cd volumes/cert/live/dagster.devwarrenarmstrong.io &&
    openssl genrsa -out privkey.pem 2048 &&
    openssl req -new -x509 -key privkey.pem -out fullchain.pem -days 365 -subj "/C=US/ST=State/L=City/O=Organization/OU=OrganizationUnit/CN=www.example.com"
)