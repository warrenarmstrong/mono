#!/bin/bash

# Define the list of entries to add
HOST_ENTRIES=(
    "127.0.0.1 pgadmin.devwarrenarmstrong.io"
    "127.0.0.1 dagster.devwarrenarmstrong.io"
)

# Loop through each entry in the list
for HOST_ENTRY in "${HOST_ENTRIES[@]}"; do
    # Check if the entry already exists in the /etc/hosts
    if ! grep -qF -- "$HOST_ENTRY" /etc/hosts; then
        # Entry does not exist, append it to /etc/hosts
        echo "$HOST_ENTRY" | sudo tee -a /etc/hosts > /dev/null
        echo "Entry added: $HOST_ENTRY"
    else
        # Entry exists, do nothing
        echo "Entry already exists: $HOST_ENTRY"
    fi
done